package br.edu.unisinos.lab2.aula6.recursao;

public class Recursao {

	public static void main(String[] args) {

		System.out.println(ehPalindromo("osssO"));

//		int[] arr1 = {1,2,3,4,5};
//		inverterArray(arr1);
//		for (int i = 0; i < arr1.length; i++) {
//			System.out.println(arr1[i]);
//		}

//		System.out.println(mdc(1440, 408));

//		System.out.println(sumAlgarism(1101));

//		int[] arr = {1,2,3,4,5};
//		System.out.println(sumArray(arr));

//		System.out.println(sumIntPar(5));

//		System.out.println(sumInt(11));	

//		System.out.println(toBinary(312323));

//		for (int i = 0; i <= 10; i++)
//			System.out.print(fibonacci(i) + " ");
	}

	public static boolean ehPalindromo(String palavra) throws IllegalArgumentException {
		if (palavra == null)
			throw new IllegalArgumentException();
		return ehPalindromoR(palavra.trim().toLowerCase(), 0, palavra.length() - 1);
	}

	private static boolean ehPalindromoR(String p, int i, int j) {
		if (i >= j)
			return true;
		return p.charAt(i) == p.charAt(j) ? ehPalindromoR(p, i + 1, j - 1) : false;
	}

	public static void inverterArray(int[] a) throws IllegalArgumentException {
		if (a == null)
			throw new IllegalArgumentException();
		inverterArrayR(a, 0, a.length - 1);
	}

	private static void inverterArrayR(int[] a, int i, int j) {
		if (i >= j)
			return;
		int aux = a[j];
		a[j] = a[i];
		a[i] = aux;
		inverterArrayR(a, i + 1, j - 1);
	}

	public static int mdc(int a, int b) throws IllegalArgumentException {
		if (a < b || a == 0 || b == 0)
			throw new IllegalArgumentException();
		return mdcR(a, b);
	}

	private static int mdcR(int a, int b) {
		return a % b == 0 ? b : mdcR(b, a % b);
	}

	public static int sumAlgarism(int n) {
		if (n < 10)
			return n;
		return (n % 10) + sumAlgarism(n / 10);
	}

	public static int sumArray(int[] arr) throws IllegalArgumentException {
		if (arr == null)
			throw new IllegalArgumentException();
		return sumArray(arr, 0);
	}

	private static int sumArray(int[] arr, int i) {
		if (i < arr.length - 1)
			return arr[i] + sumArray(arr, i + 1);
		return arr[i];
	}

	public static int sumIntPar(int n) throws IllegalArgumentException {
		if (n < 2)
			throw new IllegalArgumentException();
		return sumIntParR(n % 2 == 0 ? n : n - 1);
	}

	private static int sumIntParR(int n) {
		if (n <= 1)
			return n;
		return n + sumIntParR(n - 2);
	}

	public static int sumInt(int n) throws IllegalArgumentException {
		if (n < 1)
			throw new IllegalArgumentException();
		return sumIntR(n);
	}

	private static int sumIntR(int n) {
		if (n == 1)
			return n;
		return n + sumInt(n - 1);
	}

	public static long toBinary(long n) throws IllegalArgumentException {
		if (n < 0)
			throw new IllegalArgumentException();
		return toBin(n);
	}

	private static String toBin(int n, String bin) {
		if (n == 0)
			return bin;
		return toBin(n / 2, bin) + n % 2 + bin;
	}

	private static long toBin(long n) {
		if (n == 0)
			return n;
		return n % 2 + toBin(n / 2) * 10;
	}

	public static int fibonacci(int n) throws IllegalArgumentException {
		if (n < 0)
			throw new IllegalArgumentException();
		return fib(n);
	}

	private static int fib(int n) {
		if (n == 0 || n == 1)
			return n;
		return fib(n - 1) + fib(n - 2);
	}

}
