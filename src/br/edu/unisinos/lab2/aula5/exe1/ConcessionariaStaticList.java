package br.edu.unisinos.lab2.aula5.exe1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import br.edu.unisinos.lab2.exerciciosStaticList.StaticList;

public class ConcessionariaStaticList implements IConcessionaria {

	private StaticList<Carro> carros;

	public ConcessionariaStaticList(int capacity) {
		carros = new StaticList<>(capacity);
	}

	public void load(File file) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line, car[];
			while ((line = br.readLine()) != null) {
				car = line.split(";");
				carros.add(new Carro(car[0], car[1], Double.parseDouble(car[2]), Integer.parseInt(car[3])));
			}
		}
	}

	public void save(File file) throws IOException {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (Carro carro : carros) {
				bw.write(carro.toString());
				bw.newLine();
			}
		}
	}

	public void list() {
		if (carros.isEmpty())
			System.out.println("Não há carros cadastrados.");
		else {
			for (Carro carro : carros) {
				System.out.println(carro);
			}
		}
	}

	public boolean add(Carro car) {
		if (carros.search(car) == -1)
			return false;
		carros.add(car);
		return true; 
	}

	public Carro remove(int pos) {
		if (pos < 0 || pos >= carros.numElements())
			return null;
		return carros.remove(pos);
	}
	
}
