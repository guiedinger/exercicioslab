package br.edu.unisinos.lab2.aula5.exe1;

import java.io.File;
import java.io.IOException;

public class ConcessionariaTest {

	public static void main(String[] args) {
		
		// Instanciando uma concessionária
		IConcessionaria concessionaria = new ConcessionariaStaticList(10);

		// Carregando os carros a partir do arquivo
		try {
			concessionaria.load(new File("c:\\temp\\carros.csv"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Mostrando na tela os carros da concessionária
		concessionaria.list();

		// Adicionando um carro à lista
		concessionaria.add(new Carro("III-1111", "Ferrari", 1_500_000, 2016));

		// Mostrando na tela novamente (observar a ordem!)
		concessionaria.list();

		// Removendo o carro da posição zero
		Carro carro = concessionaria.remove(2);

		if (carro != null) {
			System.out.println("Removido: " + carro);
		} else {
			System.out.println("Carro não encontrado!");
		}

		// Salvando os carros em arquivo
		try {
			concessionaria.save(new File("c:\\temp\\carrosSaida.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
