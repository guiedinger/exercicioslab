package br.edu.unisinos.lab2.aula5.exe1;

public class Carro implements Comparable<Carro>{

	private String placa, nome;
	private double preco;
	private int ano;

	public Carro(String placa, String nome, double preco, int ano) {
		this.placa = placa;
		this.nome = nome;
		this.preco = preco;
		this.ano = ano;
	}

	public String getPlaca() {
		return placa;
	}

	@Override
	public String toString() {
		return placa + ";" + nome + ";" + preco + ";" + ano;
	}

	@Override
	public boolean equals(Object obj) {
		return ((Carro) obj).getPlaca().equals(placa);
	}

	@Override
	public int compareTo(Carro o) {
		return placa.compareTo(o.placa);
	}

}
