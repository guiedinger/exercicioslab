package br.edu.unisinos.lab2.aula5.exe1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Concessionaria implements IConcessionaria {

	private List<Carro> carros;

	public Concessionaria(int capacity) {
		carros = new ArrayList<>();
	}

	public void load(File file) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line, car[];
			while ((line = br.readLine()) != null) {
				car = line.split(";");
				carros.add(new Carro(car[0], car[1], Double.parseDouble(car[2]), Integer.parseInt(car[3])));
			}
		}
	}

	public void save(File file) throws IOException {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (Carro carro : carros) {
				bw.write(carro.toString());
				bw.newLine();
			}
		}
	}

	public void list() {
		if (carros.isEmpty())
			System.out.println("Não há carros cadastrados.");
		else {
			ordenar();
			for (Carro carro : carros) {
				System.out.println(carro);
			}
		}
	}

	public void ordenar() {
		Collections.sort(carros);
	}

	public boolean add(Carro car) {
		if (carros.contains(car))
			return false;
		return carros.add(car); 
	}

	public Carro remove(int pos) {
		if (pos < 0 || pos >= carros.size())
			return null;
		return carros.remove(pos);
	}

}
