package br.edu.unisinos.lab2.aula5.exe1;

import java.io.File;
import java.io.IOException;

public interface IConcessionaria {

	void load(File file) throws IOException;

	void save(File file) throws IOException;

	void list();

	boolean add(Carro car);

	Carro remove(int pos);

}
