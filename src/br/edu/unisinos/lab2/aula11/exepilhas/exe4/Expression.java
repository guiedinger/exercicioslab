package br.edu.unisinos.lab2.aula11.exepilhas.exe4;

import br.edu.unisinos.lab2.aula11.exepilhas.exe2.LinkedStack;
import br.edu.unisinos.prog2.exercicios.Stack;

public class Expression {

	public static void main(String[] args) {
		System.out.println("Test: eval()");
		System.out.println(eval("2.5 1 + 3 *"));				//10.5
		System.out.println(eval("2 3 ^"));						//8.0
		System.out.println(eval("5 1.8 4 / +"));				//5.45
		System.out.println(eval("4 2 5.5 * + 1 3 2 * + /"));	//2.14...
		System.out.println(eval("8 25.5 * 3.3 /"));				//61.81...
		System.out.println("Test: isProperlyParenthesized()");
		System.out.println(isProperlyParenthesized("(1 + 2) * (3 – (4 + 5))"));	//true
		System.out.println(isProperlyParenthesized("((1 + 2) * 3"));			//false
	}
	
	public static boolean isProperlyParenthesized(String exp) {
		if (exp == null || exp.length() == 0) return false;
		Stack<Character> aux = new LinkedStack<>();
		for (int i = 0; i < exp.length(); i++) { 			
			char current = exp.charAt(i);
			if(current == '(') aux.push(current);
			else if (current == ')') {
				if(aux.isEmpty()) return false;
				aux.pop();
			}
		}
		return aux.isEmpty();
	}

	
	public static double eval (String exp) {
		if (exp == null || exp.length() == 0) return 0;
		Stack<Double> aux = new LinkedStack<>();
		String value = "";
		for (int i = 0; i < exp.length(); i++) {
			char current = exp.charAt(i);
			if (isOperator(current))
				aux.push(doOperation(current, aux));
			else if (current == ' ') {
			if (value!="") 
				aux.push(Double.parseDouble(value));
			value = "";
			} else value+=current;
		}
		return aux.pop();
	}
	
	private static double doOperation(char op, Stack<Double> values) {
		double a,b;
		b = values.pop();
		a = values.pop();
		switch (op) {
		case '+':
			return a + b;
		case '-':
			return a - b;
		case '*':
			return a * b;
		case '/':
			b = b == 0 ? 1 : b;
			return a / b;
		case '^':
			return Math.pow(a, b);
		}
		return 0;
	}
	
	private static boolean isOperator(char operator) {
		char[] operators = {'+', '-', '/', '*', '^'};
		for (int i = 0; i < operators.length; i++) {
			if (operators[i]==operator) return true;
		}
		return false;
	}

}
