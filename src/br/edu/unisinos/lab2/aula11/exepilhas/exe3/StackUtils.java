package br.edu.unisinos.lab2.aula11.exepilhas.exe3;

import br.edu.unisinos.lab2.aula11.exepilhas.exe2.LinkedStack;
import br.edu.unisinos.prog2.exercicios.Stack;

public class StackUtils {
	public static void main(String[] args) {
		Stack<Integer> s = new LinkedStack<Integer>();
		s.push(1);
		s.push(2);
		s.push(3);
		Stack<Integer> reverse = reverse(s);
		System.out.println(s);//[3,2,1]
		System.out.println(reverse);//[1,2,3]
		removeAll(s);
		System.out.println(s);//[Empty]
	}
	public static <E> Stack<E> reverse(Stack<E> stack) {
		Stack<E> aux = new LinkedStack<E>();
		Stack<E> aux2 = new LinkedStack<E>();
		while (!stack.isEmpty()) {
			E current = stack.pop();
			aux.push(current);
			aux2.push(current);
		}
		while (!aux2.isEmpty()) stack.push(aux2.pop());
		
		return aux;
		
	}
	public static void removeAll(Stack<?> stack) {
		while (!stack.isEmpty()) stack.pop();
	}
}
