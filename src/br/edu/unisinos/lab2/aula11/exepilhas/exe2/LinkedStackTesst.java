package br.edu.unisinos.lab2.aula11.exepilhas.exe2;

import br.edu.unisinos.prog2.exercicios.Stack;
import br.edu.unisinos.prog2.exercicios.UnderflowException;

public class LinkedStackTesst {
	  public static void main(String[] args) {
	    Stack<Integer> s = new LinkedStack<Integer>();
	    try {
	      s.push(1);
	      s.push(2);
	      s.push(3);
	      while (!s.isEmpty()) {
	        System.out.println(s.pop());
	      }
	    } catch (UnderflowException e){
	      System.out.println(e);
	    }
	  }
	}
