package br.edu.unisinos.lab2.aula2a.exe1;

public interface Instalavel {

	public boolean instalar(Periferico periferico);

	public boolean desinstalar(Periferico periferico);

}
