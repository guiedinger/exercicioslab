package br.edu.unisinos.lab2.aula2a.exe1;

public abstract class Dispositivo {

	private int id;
	private String modelo;

	public Dispositivo(int id, String modelo) {
		this.id = id;
		this.modelo = modelo;
	}

	public int getId() {
		return id;
	}

	public String getModelo() {
		return modelo;
	}

	@Override
	public boolean equals(Object obj) {
		return ((Dispositivo) obj).getId() == id;
	}

}
