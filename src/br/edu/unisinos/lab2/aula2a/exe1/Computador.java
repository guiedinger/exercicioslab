package br.edu.unisinos.lab2.aula2a.exe1;

public class Computador extends Dispositivo implements Instalavel {

	private double velocidade;
	private int index;
	private Periferico[] perifericos;

	public Computador(int id, String modelo, double velocidade) {
		super(id, modelo);
		this.velocidade = velocidade;
		this.index = 0;
		this.perifericos = new Periferico[10];
	}

	@Override
	public boolean instalar(Periferico periferico) {
		if (index == perifericos.length)
			return false;
		for (int i = 0; i < index; i++)
			if (perifericos[i].equals(periferico))
				return false;
		perifericos[index++] = periferico;
		return true;
	}

	@Override
	public boolean desinstalar(Periferico periferico) {
		for (int i = 0; i < index; i++)
			if (perifericos[i].equals(periferico)) {
				if (i == index - 1) {
					perifericos[i] = null;
				} else {
					for (int j = i; j < index; j++)
						perifericos[j] = perifericos[j + 1];
				}
				index--;
				return true;
			}
		return false;
	}

	@Override
	public String toString() {
		String lista = "ID: " + getId() + ", Modelo: " + getModelo() + ", Velocidade: " + velocidade + "\n";
		for (int i = 0; i < index; i++)
			lista += perifericos[i] + "\n";
		return lista;
	}

}
