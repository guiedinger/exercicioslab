package br.edu.unisinos.lab2.aula2a.exe1;

public class Mouse extends Periferico {

	private int botoes;

	public Mouse(int id, String modelo, int botoes) {
		super(id, modelo);
		this.botoes = botoes;
	}

	@Override
	public String toString() {
		return "ID: " + getId() + ", Modelo: " + getModelo() + ", Botões: " + botoes;
	}

}
