package br.edu.unisinos.lab2.aula2a.exe1;

public class Impressora extends Periferico {

	private boolean color;

	public Impressora(int id, String modelo, boolean color) {
		super(id, modelo);
		this.color = color;
	}

	@Override
	public String toString() {
		return "ID: " + getId() + ", Modelo: " + getModelo() + ", Colorida: " + (color ? "Sim" : "N�o");
	}

}
