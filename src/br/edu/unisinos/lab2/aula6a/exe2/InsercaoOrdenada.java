package br.edu.unisinos.lab2.aula6a.exe2;

public class InsercaoOrdenada {

	public static void main(String[] args) {
		Integer[] a = new Integer[5];
		inserirOrdenado(a, 3);
		inserirOrdenado(a, 1);
		inserirOrdenado(a, 2);
		inserirOrdenado(a, 22);
		inserirOrdenado(a, 5);
		inserirOrdenado(a, 1);
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}

	public static void inserirOrdenado(Integer[] a, Integer n) {
		for (int i = 0; i < a.length; i++) {
			if (a[i] == null) {
				a[i] = n;
				return;
			} 
			if (a[i] > n) {
				Integer temp = a[i];
				a[i] = n;
				n = temp;
			}	
		}
	}

}
