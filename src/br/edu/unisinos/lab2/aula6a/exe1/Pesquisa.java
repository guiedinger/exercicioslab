package br.edu.unisinos.lab2.aula6a.exe1;

public class Pesquisa {

	public static void main(String[] args) {
		
		int[] a = new int[100000];
		for (int i = 0; i < a.length; i++) {
			a[i] = i;
		}
//		System.out.println("seq " + pesquisaSequencial(a, 3));
//		System.out.println("seqOr " + pesquisaSequencialOrdenada(a, 3));
//		System.out.println("bin " + pesquisaBinaria(a, 3));
		System.out.println("finger " + fingerSearch(a, 76757));
		System.out.println("bin " + pesquisaBinaria(a, 76757));

	}
	
	public static int pesquisaSequencial(int[] a, int e) {
		for (int i = 0; i < a.length; i++)
			if (a[i] == e) return i;
		return -1;
	}
	
	public static int pesquisaSequencialOrdenada(int[] a, int e) {
		for (int i = 0; i < a.length && a[i] <= e; i++)
			if (a[i] == e) return i;
		return -1;
	}
	
	public static int pesquisaBinaria(int[] a, int e) {
		int inf = 0, sup = a.length-1;
		while (inf <= sup) {
			int med = (inf + sup) / 2;
			System.out.println("inf "+inf + " sup " + sup + " med " + med);
			if (a[med] == e)
				return med;
			if (a[med] < e)
				inf = med + 1;
			else
				sup = med - 1;
		}
		return -1;
	}
	
	public static int fingerSearch(int[] a, int e) {
		int jump = 1, i = 0;
		boolean sub = true;
		while (jump > 0 && i >= 0 && i< a.length ) {
			System.out.println("i " + i + " jump " + jump);
			if (a[i] == e)
				return i;
			if (a[i] < e) {
				jump = sub ? jump+jump : jump/2;
				i = (i + jump < a.length -1) ? i + jump : a.length - 1;
			}
			else {
				jump = jump/2;
				i -= jump;
				sub = false;
			}
		}
		return -1;
	}

}
