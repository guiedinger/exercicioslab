package br.edu.unisinos.lab2.aula3a.exe1;

public class ContaCorrenteTest {

	public static void main(String[] args) {

		ContaCorrente conta1 = new ContaCorrente("Guilherme", 900_000);

		try {
			conta1.sacar(999_999);
		} catch (SaldoInsuficienteException e) {
			System.out.println(e.getMessage() + "Saldo atual: " + e.getSaldo());
		}

		try {
			conta1.depositar(0);
		} catch (ValorInvalidoException e) {
			System.out.println(e.getMessage() + "Valor da tentativa de depósito: " + e.getValor());
		}
	}

}
