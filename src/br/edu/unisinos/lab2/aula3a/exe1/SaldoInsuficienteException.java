package br.edu.unisinos.lab2.aula3a.exe1;

@SuppressWarnings("serial")
public class SaldoInsuficienteException extends Exception{

	private double saldo;
	private double valor;
	
	public SaldoInsuficienteException(String message, double saldo, double valor) {
		super(message);
		this.saldo = saldo;
		this.valor = valor;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public double getValor() {
		return valor;
	}
}
