package br.edu.unisinos.lab2.aula3a.exe1;

@SuppressWarnings("serial")
public class ValorInvalidoException extends Exception {

	private double valor;

	public ValorInvalidoException(String message, double valor) {
		super(message);
		this.valor = valor;
	}

	public double getValor() {
		return valor;
	}

}
