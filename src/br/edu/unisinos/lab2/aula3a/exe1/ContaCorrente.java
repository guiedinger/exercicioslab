package br.edu.unisinos.lab2.aula3a.exe1;

public class ContaCorrente {

	private String nomeTitular;
	private double saldo;

	public ContaCorrente(String nomeTitular, double saldo) {
		this.nomeTitular = nomeTitular;
		this.saldo = saldo;
	}

	public void sacar(double valor) throws SaldoInsuficienteException {
		if (valor > saldo)
			throw new SaldoInsuficienteException("Saldo Insuficiente.", saldo, valor);
		saldo -= valor;
	}

	public void depositar(double valor) throws ValorInvalidoException {
		if (valor <= 0)
			throw new ValorInvalidoException("Valor inválido.", valor);
		saldo += valor;
	}

	@Override
	public String toString() {
		return "ContaCorrente [nomeTitular=" + nomeTitular + ", saldo=" + saldo + "]";
	}

}
