package br.edu.unisinos.lab2.exerciciosStaticList;

import java.util.Iterator;

public class StaticList<E> implements List<E>, Iterable<E> {

	protected E[] elements;
	int numElements;

	@SuppressWarnings("unchecked")
	public StaticList(int maxNumElements) {
		elements = (E[]) new Object[maxNumElements];
		numElements = 0;
	}

	public int numElements() {
		return numElements;
	}

	public boolean isEmpty() {
		return numElements == 0;
	}

	public boolean isFull() {
		return numElements == elements.length;
	}

	@Override
	public void insert(E element, int pos) {
		if (isFull())
			throw new OverflowException();

		if (pos < 0 || pos > numElements)
			throw new IndexOutOfBoundsException();

		for (int i = numElements - 1; i >= pos; i--)
			elements[i + 1] = elements[i];

		elements[pos] = element;
		numElements++;
	}

	public void insert(List<E> list, int pos) {
		if (pos < 0 || pos > numElements)
			throw new IndexOutOfBoundsException();

		if ((numElements + list.numElements()) > elements.length)
			throw new OverflowException();

		for (int i = numElements; i > pos; i--)
			elements[i + list.numElements() - 1] = elements[i - 1];

		for (int i = 0; i < list.numElements(); i++)
			elements[pos++] = list.get(i);

		numElements = numElements + list.numElements();
	}

	public void add(E element) {
		if (isFull())
			throw new OverflowException();
		elements[numElements++] = element;
	}

	public void addAll(List<E> l) {
		if ((numElements + l.numElements()) > elements.length)
			throw new OverflowException();

		for (int i = 0; i < l.numElements(); i++)
			add(l.get(i));
	}

	public List<E> subList(int fromIndex, int toIndex) {
		if (fromIndex > toIndex)
			throw new IllegalArgumentException();
		if (fromIndex < 0 || toIndex >= numElements)
			throw new IndexOutOfBoundsException();

		int quantity = toIndex - fromIndex + 1;

		StaticList<E> subList = new StaticList<>(quantity);

		for (int i = fromIndex; i <= toIndex; i++)
			subList.add(elements[i]);

		return subList;
	}

	@Override
	public E remove(int pos) {
		if (pos < 0 || pos >= numElements)
			throw new IndexOutOfBoundsException();

		E element = elements[pos];

		for (int i = pos; i < numElements - 1; i++)
			elements[i] = elements[i + 1];

		elements[numElements - 1] = null;
		numElements--;
		return element;
	}

	public boolean remove(E element) {
		int pos = search(element);
		if (pos == -1)
			return false;
		return remove(pos) != null;
	}

	public int remove(int fromIndex, int toIndex) {
		if (fromIndex > toIndex)
			throw new IllegalArgumentException();
		if (fromIndex < 0 || toIndex >= numElements)
			throw new IndexOutOfBoundsException();

		int removeds = toIndex - fromIndex + 1;

		for (int i = toIndex; i >= fromIndex; i--)
			elements[i] = null;

		for (int i = fromIndex; i < toIndex; i++)
			if ((removeds + i) < numElements)
				elements[i] = elements[removeds + i];
			else
				break;

		numElements = numElements - removeds;

		return removeds;
	}

	public List<E> split(int pos) {
		if (pos < 0 || pos > numElements)
			throw new IndexOutOfBoundsException();

		List<E> rightList = new StaticList<>(numElements);

		int oldNumElements = numElements;

		for (int i = pos; i < oldNumElements; i++)
			rightList.insert(remove(pos), rightList.numElements());

		return rightList;
	}

	public E get(int pos) {
		if (pos < 0 || pos >= numElements)
			throw new IndexOutOfBoundsException();
		return elements[pos];
	}

	public void reverse() {
		for (int i = 0; i < (numElements) / 2; i++) {
			E aux = elements[i];
			elements[i] = elements[numElements - i - 1];
			elements[numElements - i - 1] = aux;
		}
	}

	public int search(E element) {
		for (int i = 0; i < numElements; i++)
			if (element.equals(elements[i]))
				return i;
		return -1;
	}

	public boolean equals(List<E> list) {
		if (list == null)
			return false;
		if (numElements != list.numElements())
			return false;

		for (int i = 0; i < numElements; i++)
			if (!get(i).equals(list.get(i)))
				return false;

		return true;
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			int count = 0;
			@Override
			public boolean hasNext() {
				return count < numElements;
			}

			@Override
			public E next() {
				if (!hasNext())
					throw new RuntimeException("List is empty");
				return elements[count++];
			}
		};
	}

	public String toString() {
		String s = "";
		for (int i = 0; i < numElements; i++)
			s += elements[i] + " ";
		return s;
	}

}
