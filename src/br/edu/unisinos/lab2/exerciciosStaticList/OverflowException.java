package br.edu.unisinos.lab2.exerciciosStaticList;

public class OverflowException extends RuntimeException {
	  private static final long serialVersionUID = 1L;

	  public OverflowException() {
	    super("Overflow!");
	  }
	}
