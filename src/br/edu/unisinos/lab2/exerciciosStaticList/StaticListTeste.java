package br.edu.unisinos.lab2.exerciciosStaticList;

public class StaticListTeste {
	public static void main(String[] args) {
		StaticList<Integer> lista = new StaticList<>(10);
		for (int i = 0; i < 10; i++) {
			lista.insert(i * 2, i);
		}

//		Integer num = lista.get(5);
//		System.out.println("Lista após inserções: ");
//		System.out.println(lista);
//		System.out.println(lista.remove(num) + " foi removido da lista!");
//		System.out.println("\nLista após remoção do elemento da posição 5:");
//		lista.add(5);
//		lista.reverse();
//		System.out.println("Lista reversa");
//		lista.remove(0, 6);
		System.out.println(lista);
		List<Integer> novaLista = lista.subList(0, 9);
		System.out.println("left");
		System.out.println(lista);
		System.out.println("right");
		System.out.println(novaLista);
		System.out.println(lista.equals(novaLista));
		for (Integer integer : lista) {
			System.out.print(integer + " - ");
		}
		
//		System.out.println("Insert");
//		System.out.println(lista);
	}
}
