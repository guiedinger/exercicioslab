package br.edu.unisinos.lab2.aula4.exe1;

import java.io.File;
import java.io.IOException;

public interface IBookCatalog {

	void load(File file) throws IOException;

	void save(File file) throws IOException;

	void list();

	void add(Book book);

	void remove(int pos);
}
