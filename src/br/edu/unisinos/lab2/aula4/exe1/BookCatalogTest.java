package br.edu.unisinos.lab2.aula4.exe1;

import java.io.File;
import java.io.IOException;

public class BookCatalogTest {

	public static void main(String[] args) {
		IBookCatalog bc = new BookCatalog(2);
		try {
			bc.load(new File("C:/temp/books.txt"));
		} catch (IOException e) {
			System.out.println("Arquivo Inválido.");
		}
		bc.list();
		bc.remove(1);
		bc.add(new Book("Narnia", "9788970137509", "Abril", "www.abril.com", 2.3));
		bc.list();
		try {
			bc.save(new File("C:/temp/newBooks.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
