package br.edu.unisinos.lab2.aula4.exe1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class BookCatalog implements IBookCatalog {

	private Book[] books;
	private int index;

	public BookCatalog(int tamanho) {
		books = new Book[tamanho];
		index = 0;
	}

	public void load(File file) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String linha, book[];
			while ((linha = br.readLine()) != null) {
				if (index < books.length) {
					book = linha.split(";");
					books[index++] = new Book(book[0], book[1], book[2], book[3], Double.parseDouble(book[4]));
				} else {
					System.out.println("O arquivo tem mais livros que o tamanho da lista.");
					break;
				}
			}
		}
	}

	public void list() {
		if (index == 0)
			System.out.println("Não há livros cadastrados!");
		for (int i = 0; i < index; i++)
			System.out.println(books[i]);
	}

	public void add(Book book) {
		if (index == books.length) {
			System.out.println("Lista cheia.");
			return;
		}
		books[index++] = book;
	}

	public void remove(int pos) {
		if (index == books.length - 1)
			if (books[pos] != null)
				books[pos] = null;
			else
				System.out.println("Não há livro nessa posição.");
		else if (pos >= 0 && pos < index)
			if (books[pos] != null)
				for (int i = pos; i < index - 1; i++)
					books[i] = books[i + 1];
			else
				System.out.println("Não há livro nessa posição.");
		else {
			System.out.println("Posição inválida.");
			index++;
		}
		index--;
	}

	public void save(File file) throws IOException {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (int i = 0; i < index; i++)
				bw.write(books[i].toFileFormat());
		}
	}

}
