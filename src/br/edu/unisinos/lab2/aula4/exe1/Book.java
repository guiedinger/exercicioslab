package br.edu.unisinos.lab2.aula4.exe1;

public class Book {

	private String titulo;
	private String isbn;
	private String editora;
	private String url;
	private double preco;

	public Book(String titulo, String isbn, String editora, String url, double preco) {
		super();
		this.titulo = titulo;
		this.isbn = isbn;
		this.editora = editora;
		this.url = url;
		this.preco = preco;
	}

	@Override
	public String toString() {
		return "Book [titulo=" + titulo + ", isbn=" + isbn + ", editora=" + editora + ", url=" + url + ", preco="
				+ preco + "]";
	}

	public String toFileFormat() {
		return titulo + ";" + isbn + ";" + editora + ";" + url + ";" + preco + "\n";
	}

}
