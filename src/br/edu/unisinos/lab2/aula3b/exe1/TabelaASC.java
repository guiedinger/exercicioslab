package br.edu.unisinos.lab2.aula3b.exe1;

public class TabelaASC {

	public static void main(String[] args) {
		
		for (int i = 0; i < 32; i++) {
			for (int j = 0; j < 255; j += 32) {
				System.out.print(i + j + ": " + (i+j == 9 || i+j == 10 || i+j == 13 ? " " : (char)(i+j)) + "\t");
			}
			System.out.println("");
		}

	}

}
