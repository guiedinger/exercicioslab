package br.edu.unisinos.lab2.aula1.exe3;

public interface Autenticavel {
	
	public boolean autenticar(int senha);

}
