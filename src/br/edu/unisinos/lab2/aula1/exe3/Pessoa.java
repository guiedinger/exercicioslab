package br.edu.unisinos.lab2.aula1.exe3;

public abstract class Pessoa {
	
	protected String nome;
	protected int senha;

	public Pessoa(String nome, int senha) {
		this.nome = nome;
		this.senha = senha;
	}

	public abstract void exibirDados();
}
