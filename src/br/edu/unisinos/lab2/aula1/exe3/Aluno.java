package br.edu.unisinos.lab2.aula1.exe3;

public class Aluno extends Pessoa implements Autenticavel {

	public Aluno(String nome, int senha) {
		super(nome, senha);
	}

	@Override
	public void exibirDados() {
		System.out.println("Nome: " + nome + ", Senha: " + senha);
	}

	@Override
	public boolean autenticar(int senha) {
		return this.senha==senha;
	}

}
