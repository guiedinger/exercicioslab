package br.edu.unisinos.lab2.aula1.exe2;

public interface IFigura {

	public double calcularArea();

	public double calcularPerimetro();

}
