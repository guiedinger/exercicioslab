package br.edu.unisinos.lab2.aula1.exe2;

public class FiguraTest {

	public static void main(String[] args) {
		IFigura retangulo = new Retangulo(2, 2);
		IFigura circulo = new Circulo(2);

		System.out.printf("Retangulo\nArea: %12.2f Perimetro: %12.2f\n", retangulo.calcularArea(),
				retangulo.calcularPerimetro());
		System.out.printf("Circulo\nArea: %12.2f Perimetro: %12.2f\n", circulo.calcularArea(),
				circulo.calcularPerimetro());

	}

}
