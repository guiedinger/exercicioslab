package br.edu.unisinos.lab2.aula1.exe2;

public class Retangulo implements IFigura {

	private double base;
	private double altura;

	public Retangulo(double base, double altura) {
		this.base = base;
		this.altura = altura;
	}

	@Override
	public double calcularArea() {
		return base * altura;
	}

	@Override
	public double calcularPerimetro() {
		return base * 2 + altura * 2;
	}

}
