package br.edu.unisinos.lab2.aula1.exe1;

public class Violao extends InstrumentoCorda {

	public Violao(int nroCordas) {
		super("Viol�o", nroCordas);
	}

	@Override
	public void exibirDados() {
		System.out.printf("Nome = %s Nro de Cordas = %d", nome, nroCordas);
	}

}
