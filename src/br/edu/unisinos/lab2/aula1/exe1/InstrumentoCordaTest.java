package br.edu.unisinos.lab2.aula1.exe1;

public class InstrumentoCordaTest {

	public static void main(String[] args) {
		InstrumentoCorda guitarra = new Guitarra(12);
		guitarra.exibirDados();

		InstrumentoCorda violao = new Violao(6);
		violao.exibirDados();

	}

}
