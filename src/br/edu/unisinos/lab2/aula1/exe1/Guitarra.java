package br.edu.unisinos.lab2.aula1.exe1;

public class Guitarra extends InstrumentoCorda {

	public Guitarra(int nroCordas) {
		super("Guitarra", nroCordas);
	}

	@Override
	public void exibirDados() {
		System.out.printf("Nome = %s Nro de Cordas = %d", nome, nroCordas);
	}
}
