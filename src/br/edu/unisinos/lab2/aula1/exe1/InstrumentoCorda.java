package br.edu.unisinos.lab2.aula1.exe1;

public abstract class InstrumentoCorda {
	protected String nome;
	protected int nroCordas;
	
	public InstrumentoCorda(String nome, int nroCordas) {
		this.nome = nome;
		this.nroCordas = nroCordas;
	}
	
	public abstract void exibirDados();
}
