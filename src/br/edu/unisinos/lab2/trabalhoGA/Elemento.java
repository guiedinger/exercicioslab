package br.edu.unisinos.lab2.trabalhoGA;

public abstract class Elemento {

	private char caractere;
	private String nome;
	private int unidades;

	public Elemento(char caractere, String nome, int unidades) {
		this.caractere = caractere;
		this.nome = nome;
		this.unidades = unidades;
	}

	public void adicionarUnidade() {
		unidades++;
	}

	@Override
	public String toString() {
		return nome + "\t(" + caractere + ") que tem " + unidades + " unidade(s)";
	}

}
