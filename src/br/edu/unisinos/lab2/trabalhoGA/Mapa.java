package br.edu.unisinos.lab2.trabalhoGA;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Mapa implements IMapa {

	private char[][] mapa;
	private boolean[][] isVisitado;
	private Elemento[] elementos;
	private int index, linhasMapa, colunasMapa;

	@Override
	public void carregar(File file) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String linha = br.readLine();
			linhasMapa = Integer.parseInt(linha.split(" ")[0]);
			colunasMapa = Integer.parseInt(linha.split(" ")[1]);
			mapa = new char[linhasMapa][colunasMapa];
			isVisitado = new boolean[linhasMapa][colunasMapa];
			elementos = new Elemento[linhasMapa * colunasMapa];
			for (int i = 0; i < mapa.length; i++) {
				linha = br.readLine();
				for (int j = 0; j < mapa[i].length; j++)
					mapa[i][j] = linha.charAt(j);
			}
		} catch (StringIndexOutOfBoundsException | NullPointerException e) {
			System.out.println("Arquivo de Mapa Inválido.");
		}
	}

	@Override
	public void reconhecer() {
		for (int i = 0; i < mapa.length; i++)
			for (int j = 0; j < mapa[i].length; j++) {
				if (!isVisitado[i][j]) {
					int unidadesElemento = reconhecerElemento(mapa[i][j], i, j);
					switch (mapa[i][j]) {
					case '=':
						elementos[index++] = new Cerca('=', unidadesElemento);
						break;
					case '*':
						elementos[index++] = new Agua('*', unidadesElemento);
						break;
					case '+':
						elementos[index++] = new Arvore('+', unidadesElemento);
						break;
					case '/':
						elementos[index++] = new Edificacao('/', unidadesElemento);
						break;
					case ' ':
						elementos[index++] = new Campo(' ', unidadesElemento);
						break;
					default:
						elementos[index++] = new Desconhecido(mapa[i][j], unidadesElemento);
						break;
					}
				}
			}
	}

	public int reconhecerElemento(char caractere, int linha, int coluna) {
		if (linha >= 0 && coluna >= 0 && linha < linhasMapa && coluna < colunasMapa && !isVisitado[linha][coluna]
				&& mapa[linha][coluna] == caractere) {
			isVisitado[linha][coluna] = true;
			return 1 + reconhecerElemento(caractere, linha - 1, coluna)
					+ reconhecerElemento(caractere, linha + 1, coluna)
					+ reconhecerElemento(caractere, linha, coluna - 1)
					+ reconhecerElemento(caractere, linha, coluna + 1);
		}
		return 0;
	}

	@Override
	public void exibirMatriz() {
		for (int i = 0; i < mapa.length; i++) {
			for (int j = 0; j < mapa[i].length; j++)
				System.out.print(mapa[i][j]);
			System.out.println("");
		}
	}

	@Override
	public void exibirElementos() {
		System.out.println("\nRelatório do Reconhecimento\n" + "===========================");
		for (int i = 0; i < index; i++) {
			System.out.println(elementos[i]);
		}
	}

}
