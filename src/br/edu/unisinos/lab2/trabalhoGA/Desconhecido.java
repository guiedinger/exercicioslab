package br.edu.unisinos.lab2.trabalhoGA;

public class Desconhecido extends Elemento {

	public Desconhecido(char caractere, int unidades) {
		super(caractere, "Desconhecido", unidades);
	}

}
