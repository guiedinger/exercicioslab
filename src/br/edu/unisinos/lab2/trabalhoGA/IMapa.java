package br.edu.unisinos.lab2.trabalhoGA;

import java.io.File;
import java.io.IOException;

public interface IMapa {
	
	public void carregar(File file) throws IOException;

	public void reconhecer();

	public void exibirMatriz();

	public void exibirElementos();
	
}
