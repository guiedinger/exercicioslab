package br.edu.unisinos.prog2.exercicios;

public class UnderflowException extends RuntimeException {
	public UnderflowException() {
		super("Underflow!");
	}

}