package br.edu.unisinos.prog2.exercicios;

public class OverflowException extends RuntimeException {
	public OverflowException() {
		super("Overflow!");
	}

}
