package br.edu.unisinos.prog2.exercicios;


public class ExercicioPilha {

	public static void main(String[] args) {
		
		
		
		
		
	}
	
	public static void prependQueue(Queue<Integer> q1, Queue<Integer> q2) throws OverflowException, NullPointerException {
		if (q1 == null || q2 == null) throw new NullPointerException();
		Queue<Integer> aux = new StaticQueue<>(q1.numElements());
		while (!q1.isEmpty())
			aux.enqueue(q1.dequeue());
		while (!q2.isEmpty())
			q1.enqueue(q2.dequeue());
		while (!aux.isEmpty())
			q1.enqueue(aux.dequeue());
	}
	
	public static void exercicio1(Stack<Integer> s) {
		Stack<Integer> neg = new StaticStack<>(s.numElements());
		Stack<Integer> pos = new StaticStack<>(s.numElements());
		Stack<Integer> zero = new StaticStack<>(s.numElements());
		
		while (!s.isEmpty()) {
			int aux = s.pop();
			if(aux > 0)
				pos.push(aux);
			else if (aux < 0)
				neg.push(aux);
			else
				zero.push(aux);
		}
		
		while (!pos.isEmpty())
			s.push(pos.pop());
		while (!zero.isEmpty())
			s.push(zero.pop());
		while (!neg.isEmpty())
			s.push(neg.pop());
	}

}
