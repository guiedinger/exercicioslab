package br.edu.unisinos.prog2.recursao.lista2;

public class Lista2 {

	public static void main(String[] args) {
		//System.out.println(retornaMediaInteiro(0));
//		String [] arr = null;
//		System.out.println(qtdPalindromo(arr));
		//System.out.println(ehPerfeito(8129));
		//System.out.println(numeroDigitos(10000, 0));
	}
	//09
	public static int numeroDigitos(int n, int d) throws IllegalArgumentException{
		if (n <= 0 || d >= 10 || d < 0) throw new IllegalArgumentException();
		return numeroDigitosR(n, d);
	}
	
	private static int numeroDigitosR(int n, int d) {
		if (n < 10) return n == d ? 1 : 0;
		if (n % 10 == d) return numeroDigitosR(n/10, d) + 1;
		return numeroDigitosR(n/10, d);
	}
	//11
	public static boolean ehPerfeito(int n) throws  IllegalArgumentException {
		if (n <= 0) throw new IllegalArgumentException();
		return ehPerfeitoR(n, 1) == n;
	}
	private static int ehPerfeitoR(int n, int i) {
		if (i == n) return 0;
		if (n%i == 0) return i + ehPerfeitoR(n, i + 1);
		return ehPerfeitoR(n, i + 1);
	}
	//07
	public static boolean ehPalindromo(String palavra) throws IllegalArgumentException {
		if (palavra == null)
			throw new IllegalArgumentException();
		return ehPalindromoR(palavra.trim().toLowerCase(), 0, palavra.length() - 1);
	}

	private static boolean ehPalindromoR(String p, int i, int j) {
		if (i >= j)
			return true;
		return p.charAt(i) == p.charAt(j) ? ehPalindromoR(p, i + 1, j - 1) : false;
	}
	//8
	public static int qtdPalindromo(String[] arr) throws IllegalArgumentException {
		if (arr == null) throw new IllegalArgumentException();
		return qtdPalindromoR(arr, 0);
	}
	
	private static int qtdPalindromoR(String[] arr, int i) {
		if (i >= arr.length) return 0;
		if (ehPalindromo(arr[i])) return qtdPalindromoR(arr, i + 1) + 1;
		return qtdPalindromoR(arr, i+1);
	}
	//10
	public static double retornaMediaInteiro(int num) throws IllegalArgumentException {
		if (num <= 0) throw new IllegalArgumentException();
		return retornaMediaInteiroR(num, 1, 0);
	}
	
	private static double retornaMediaInteiroR(int num, int i, double soma) {
		if (num < 10) return (num+soma)/i;
		return retornaMediaInteiroR(num/10, i + 1, soma + num%10);
	}
}
