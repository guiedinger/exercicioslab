package br.edu.unisinos.prog2.recursao.lista1;

public class Lista1 {
	
	public void imprimeArrayReverso(int[] v) throws IllegalArgumentException{
		if (v == null) throw new IllegalArgumentException();
		pArrayR(v, 0);
	}
	
	private void pArrayR(int[] v, int pos) {
		if (pos < v.length) {
			pArrayR(v, pos + 1);
			System.out.print(v[pos]);			
		}
	}
	
	public double somaArrayBi(double[][] v) throws IllegalArgumentException{
		if (v == null) throw new IllegalArgumentException();
		return somaBiR(v, 0, 0);
	}
	
	private double somaBiR(double[][] v, int i, int j) {
		if (i >= v.length)
			return 0;
		if (j >= v[i].length)
			return somaBiR(v, ++i, 0);
		return v[i][j] + somaBiR(v, i, ++j);
	}
	
	public int encontraPosicaoMenorValor(int[] v) throws IllegalArgumentException{
		if (v == null || v.length == 0) throw new IllegalArgumentException();
		return encontraPosR(v, 0, 0);
	}
	
	private int encontraPosR(int[] v, int menor, int pos) {
		if (pos < v.length)
			return encontraPosR(v, v[pos] < v[menor] ?  pos : menor, ++pos);
		return menor;
	}
	
	public void printPrimaryDiagonal(int[][] v) throws MatrixNotPossibleException{
		if (v == null || v.length == 0) throw new MatrixNotPossibleException();
		if (v.length != v[0].length) throw new MatrixNotPossibleException();
		printD(v, 0, 0);
	}
	
	private void printD(int[][] v, int i, int j) {
		if (i >= v.length) return;
		System.out.println(v[i][j]);
		printD(v, ++i, ++j);
	}
	
	public int binaryToDecimal(String binary) throws IllegalArgumentException{
		if (binary.length() != 8) throw new IllegalArgumentException();
		return binaryToDR(binary, 0);
	}
	
	private int binaryToDR(String b, int pos) {
		if (b.charAt(pos) == '1' && pos < 7)
			return (int) (Math.pow(2, 7 - pos) + binaryToDR(b, pos + 1));
		else if (pos == 7 && b.charAt(pos) == '1') return 1;
		else if (pos == 7 && b.charAt(pos) != '1') return 0;
		return binaryToDR(b, pos + 1);
	}
	public static void main(String[] args) {
		
		Lista1 l = new Lista1();
		
		int[] v1 = {1,2,3,4,5,6,7};
		//l.imprimeArrayReverso(v1);
		double[][] v2 = {{1,2,3,4,},
				{1,2,3,4,},
				{1,2,5.5,4,},
				{1,2,3,4,}};
		System.out.println(l.somaArrayBi(v2));
		int[] v3 = {23, 35, 16, 8, 13, 11, 10, 20, 9, 12};
		//System.out.println(l.encontraPosicaoMenorValor(v3));
		
		//l.printPrimaryDiagonal(v2);
		//System.out.println(l.binaryToDecimal("11010000"));
		

	}

}
