package br.edu.unisinos.prog2.revisaoProvaGrauA;

public class OverflowException extends RuntimeException {
	public OverflowException() {
		super("Overflow!");
	}

}