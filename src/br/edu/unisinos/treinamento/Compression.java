package br.edu.unisinos.treinamento;

public class Compression {

	public static void main(String[] args) {
		System.out.println(compressString("AAABBBBBBBCC")); // A3B7C3
	}

	public static String compressString(String text) {
		String newText = "";
		for (int i = 0; i < text.length(); i++) {
			int sequences = countSequence(text, text.charAt(i), i + 1);
			newText += text.charAt(i) + (sequences > 0 ? ((sequences + 1) + "") : "");
			i += sequences;
		}
		return newText;
	}

	private static int countSequence(String text, char current, int pos) {
		if (pos == text.length())
			return 0;
		if (current == text.charAt(pos))
			return countSequence(text, current, pos + 1) + 1;
		return 0;
	}

}
