package br.edu.unisinos.lab1.avaliacaoGA;

import auxiliares.Teclado;

public class Pc {
	private String marca, modelo, marcaCpu, modeloCpu;

	private double preco;

	private int quantidadeRam, tamanhoHd, sistemaOperacional, quantidadePortasUsb;

	private boolean comDvd, formatado;

	public void leDados() {
		Teclado leitor = new Teclado();
		this.setMarca(leitor.leString("Qual a marca?"));
		this.setModelo(leitor.leString("Qual o modelo?"));
		this.setMarcaCpu(leitor.leString("Qual a marca da CPU?"));
		this.setModeloCpu(leitor.leString("Qual o modelo da CPU?"));
		this.setPreco(leitor.leDouble("Qual o pre�o?"));
		do {
			this.setQuantidadeRam(leitor.leInt("Quanto de memoria RAM?\n" + "Entre 2 at� 32:"));
		} while (!(this.quantidadeRam >= 2 && quantidadeRam <= 32));

		do {
			this.setTamanhoHd(leitor.leInt("Qual o tamanho do HD?\n" + "Entre 250 at� 2000:"));
		} while (!(tamanhoHd >= 250 && tamanhoHd <= 2000));

		do {
			this.setSistemaOperacional(leitor
					.leInt("Qual o sistema operacional?\n" + "Digite 1 para Windows, 2 para Linux e 3 para MacOS:"));
		} while (!(sistemaOperacional >= 1 && sistemaOperacional <= 3));

		do {
			this.setQuantidadePortasUsb(leitor.leInt("Quantas portas USB?\n" + "Entre 1 at� 5:"));
		} while (!(quantidadePortasUsb >= 1 && quantidadePortasUsb <= 5));

		this.setComDvd(leitor.leString("Tem DVD?\n" + "Digite 'sim' ou 'nao':").equalsIgnoreCase("sim"));
		this.setFormatado(leitor.leString("Foi formatado?\n" + "Digite 'sim' ou 'nao':").equalsIgnoreCase("sim"));
	}

	public String retornaDados() {
		return "\nMarca: " + this.marca + "\nModelo: " + this.modelo + "\nMarca CPU: " + this.marcaCpu
				+ "\nModelo CPU: " + this.modeloCpu + "\nPre�o: " + this.preco + "\nQuantidade de RAM: "
				+ this.quantidadeRam + "\nTamanho HD: " + this.tamanhoHd + "\nSistema Operacional: "
				+ (this.sistemaOperacional == 1 ? "Windows"
						: this.sistemaOperacional == 2 ? "Linux" : this.sistemaOperacional == 3 ? "MacOS" : "")
				+ "\nQuantidade de porta USB: " + this.quantidadePortasUsb + "\nDVD: " + (this.comDvd ? "Sim" : "N�o")
				+ "\nFormatado: " + (this.formatado ? "Sim" : "N�o");
	}

	public Pc() {
	}

	public Pc(String marca, String modelo, String marcaCpu, String modeloCpu, double preco, int quantidadeRam,
			int tamanhoHd, int sistemaOperacional, int quantidadePortasUsb, boolean comDvd, boolean formatado) {
		this.marca = marca;
		this.modelo = modelo;
		this.marcaCpu = marcaCpu;
		this.modeloCpu = modeloCpu;
		this.preco = preco;
		this.quantidadeRam = quantidadeRam;
		this.tamanhoHd = tamanhoHd;
		this.sistemaOperacional = sistemaOperacional;
		this.quantidadePortasUsb = quantidadePortasUsb;
		this.comDvd = comDvd;
		this.formatado = formatado;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMarcaCpu() {
		return marcaCpu;
	}

	public void setMarcaCpu(String marcaCpu) {
		this.marcaCpu = marcaCpu;
	}

	public String getModeloCpu() {
		return modeloCpu;
	}

	public void setModeloCpu(String modeloCpu) {
		this.modeloCpu = modeloCpu;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public int getQuantidadeRam() {
		return quantidadeRam;
	}

	public void setQuantidadeRam(int quantidadeRam) {
		if (quantidadeRam >= 2 && quantidadeRam <= 32) {
			this.quantidadeRam = quantidadeRam;
		} else {
			System.out.println("Valor inv�lido!");
		}
	}

	public int getTamanhoHd() {
		return tamanhoHd;
	}

	public void setTamanhoHd(int tamanhoHd) {
		if (tamanhoHd >= 250 && tamanhoHd <= 2000) {
			this.tamanhoHd = tamanhoHd;
		} else {
			System.out.println("Valor inv�lido!");
		}
	}

	public int getSistemaOperacional() {
		return sistemaOperacional;
	}

	public void setSistemaOperacional(int sistemaOperacional) {
		if (sistemaOperacional >= 1 && sistemaOperacional <= 3) {
			this.sistemaOperacional = sistemaOperacional;
		} else {
			System.out.println("Valor inv�lido!");
		}
	}

	public int getQuantidadePortasUsb() {
		return quantidadePortasUsb;
	}

	public void setQuantidadePortasUsb(int quantidadePortasUsb) {
		if (quantidadePortasUsb >= 1 && quantidadePortasUsb <= 5) {
			this.quantidadePortasUsb = quantidadePortasUsb;
		} else {
			System.out.println("Valor inv�lido!");
		}
	}

	public boolean isComDvd() {
		return comDvd;
	}

	public void setComDvd(boolean comDvd) {
		this.comDvd = comDvd;
	}

	public boolean isFormatado() {
		return formatado;
	}

	public void setFormatado(boolean formatado) {
		this.formatado = formatado;
	}

}
