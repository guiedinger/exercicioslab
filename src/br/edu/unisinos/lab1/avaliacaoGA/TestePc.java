package br.edu.unisinos.lab1.avaliacaoGA;

public class TestePc {

	public static void main(String[] args) {
		Pc comp1 = new Pc();
		comp1.setMarca("Apple");
		comp1.setModelo("MacBook Air");
		comp1.setMarcaCpu("Intel");
		comp1.setModeloCpu("i5");
		comp1.setPreco(8000);
		comp1.setQuantidadeRam(16);
		comp1.setTamanhoHd(480);
		comp1.setSistemaOperacional(3);
		comp1.setQuantidadePortasUsb(2);
		comp1.setComDvd(false);
		comp1.setFormatado(false);
		Pc comp2 = new Pc("Lenovo","Z40","Intel","i5",2300,8,1000,1,3,true,true);
		Pc comp3 = new Pc();
		comp3.leDados();
		
		System.out.println(comp1.retornaDados());
		System.out.println(comp2.retornaDados());
		System.out.println(comp3.retornaDados());
		
		int maiorRam, maiorHd;
		maiorRam = comp1.getQuantidadeRam();
		maiorHd = comp1.getTamanhoHd();
		
		if(comp2.getQuantidadeRam() > maiorRam) {
			maiorRam = comp2.getQuantidadeRam();
		}
		
		if(comp3.getQuantidadeRam() > maiorRam) {
			maiorRam = comp3.getQuantidadeRam();
		}
		
		if(comp2.getTamanhoHd() > maiorHd) {
			maiorHd = comp2.getTamanhoHd();
		}
		
		if(comp3.getTamanhoHd() > maiorHd) {
			maiorHd = comp3.getTamanhoHd();
		}
		
		System.out.println("\nModelos com mais mem�ria RAM, com: " + maiorRam + "GB");
		if(comp1.getQuantidadeRam() == maiorRam){
			System.out.println(comp1.getMarca() + " " + comp1.getModelo());
		}
		
		if(comp2.getQuantidadeRam() == maiorRam){
			System.out.println(comp2.getMarca() + " " + comp2.getModelo());
		}
		
		if(comp3.getQuantidadeRam() == maiorRam){
			System.out.println(comp3.getMarca() + " " + comp3.getModelo());
		}
		
		System.out.println("\nModelos com o maior HD, com: " + maiorHd + "GB");
		if(comp1.getTamanhoHd() == maiorHd){
			System.out.println(comp1.getMarca() + " " + comp1.getModelo());
		}
		
		if(comp2.getTamanhoHd() == maiorHd){
			System.out.println(comp2.getMarca() + " " + comp2.getModelo());
		}
		
		if(comp3.getTamanhoHd() == maiorHd){
			System.out.println(comp3.getMarca() + " " + comp3.getModelo());
		}
		
		
	}

}
