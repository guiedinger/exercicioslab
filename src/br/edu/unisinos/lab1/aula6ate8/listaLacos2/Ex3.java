package br.edu.unisinos.lab1.aula6ate8.listaLacos2;

import auxiliares.Teclado;

public class Ex3 {

	public static void main(String[] args) {
		Teclado leitor = new Teclado();
		int n, m, media, nTemp;
		boolean isIncreasing = true;
		
		n = leitor.leInt("Altura: ");
		m = leitor.leInt("Largura: ");
		media = m/2;
		nTemp=0;
		
		//losangulo
		System.out.println("Losangulo: \n\n");
		for (int i = 0; i < n; i++) {
			if(nTemp >= media && isIncreasing){
				isIncreasing = false;
			} else if(nTemp < 1) {
				isIncreasing = true;
			}
			nTemp = isIncreasing ? nTemp + 1 : nTemp-1;
			for (int j = 0; j < m; j++) {
				if(j > (media - nTemp) && j < (media + nTemp)){
					System.out.print("*");					
				} else { System.out.print(" ");}
			}
			System.out.println("");
		}
		
		
		//piramide
		System.out.println("Piramide: \n\n");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if(j > (media - i) && j < (media + i)){
					System.out.print("*");					
				} else { System.out.print(" ");}
			}
			System.out.println("");
		}
		
		
		
		//retangulo
		System.out.println("Retangulo: \n\n");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				System.out.print("*");
			}
			System.out.println("");
		}

	}

}
