package br.edu.unisinos.lab1.aula6ate8.trabalhoGa;

import auxiliares.Teclado;

public class TesteEmail {

	public static void main(String[] args) {
		Teclado leitor = new Teclado();
		
		Email email1 = new Email(leitor.leString("Remetente: "),leitor.leString("Destinatario: "),leitor.leString("Assunto: "),leitor.leString("Corpo: "),leitor.leInt());
		Email email2 = new Email(leitor.leString("Remetente: "),leitor.leString("Destinatario: "),leitor.leString("Assunto: "),leitor.leString("Corpo: "),leitor.leInt());
		Email email3 = new Email(leitor.leString("Remetente: "),leitor.leString("Destinatario: "),leitor.leString("Assunto: "),leitor.leString("Corpo: "),leitor.leInt());

		int maior, menor;
		maior = email1.getPrioridade();
		menor = email1.getPrioridade();
		
		//maior
		if(email2.getPrioridade() < maior) {
			maior = email2.getPrioridade();
		}
		if(email3.getPrioridade() < maior) {
			maior = email3.getPrioridade();
		}
		
		//menor
		if(email2.getPrioridade() > menor) {
			menor = email2.getPrioridade();
		}
		if(email3.getPrioridade() > menor) {
			menor = email3.getPrioridade();
		}
		
		//print
		System.out.println("\nAlta prioridade:");
		if(email1.getPrioridade() == maior){
			email1.mostraEmail();
		}
		if(email2.getPrioridade() == maior){
			email2.mostraEmail();
		}
		if(email3.getPrioridade() == maior){
			email3.mostraEmail();
		}
		System.out.println("\nBaixa prioridade:");
		if(email1.getPrioridade() == menor){
			email1.mostraEmail();
		}
		if(email2.getPrioridade() == menor){
			email2.mostraEmail();
		}
		if(email3.getPrioridade() == menor){
			email3.mostraEmail();
		}
		
		System.out.println("\nRemetente:\n");
		
		//1
		if(email1.getRemetente().equalsIgnoreCase(email2.getDestinatario())){
			System.out.println("Remetente " + email1.getRemetente() + " � destinat�io do email enviado por " + email2.getRemetente());
		}
		if(email1.getRemetente().equalsIgnoreCase(email3.getDestinatario())){
			System.out.println("Remetente " + email1.getRemetente() + " � destinat�io do email enviado por " + email3.getRemetente());
		}
		//2
		if(email2.getRemetente().equalsIgnoreCase(email1.getDestinatario())){
			System.out.println("Remetente " + email2.getRemetente() + " � destinat�io do email enviado por " + email1.getRemetente());
		}
		if(email2.getRemetente().equalsIgnoreCase(email3.getDestinatario())){
			System.out.println("Remetente " + email2.getRemetente() + " � destinat�io do email enviado por " + email3.getRemetente());
		}
		//3
		if(email3.getRemetente().equalsIgnoreCase(email1.getDestinatario())){
			System.out.println("Remetente " + email3.getRemetente() + " � destinat�io do email enviado por " + email1.getRemetente());
		}
		if(email3.getRemetente().equalsIgnoreCase(email2.getDestinatario())){
			System.out.println("Remetente " + email3.getRemetente() + " � destinat�io do email enviado por " + email2.getRemetente());
		}

	}

}
