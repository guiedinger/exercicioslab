package br.edu.unisinos.lab1.aula6ate8.trabalhoGa;

public class Email {
	
	private String remetente,
					destinatario,
					assunto,
					corpo;
	private int prioridade;
	private boolean ehAssinado = false;
	
	public boolean verificarEmail() {
		if(
				this.remetente.equals("") || 
				this.destinatario.equals("") ||
				this.assunto.equals("") ||
				this.corpo.equals("") ||
				this.remetente.equalsIgnoreCase(this.destinatario) ||
				this.prioridade < 1 ||
				this.prioridade > 20){
			return true;
		}
		return false;
	}
	
	public void mostraEmail() {
		System.out.println("\nAssunto: " + this.assunto +
							"\nCorpo: " + this.corpo +
							"\nDestinatario: " + this.destinatario +
							"\nRemetente: " + this.remetente + 
							"\nPrioridade: " + this.prioridade);
	}
	
	public String serializaEmail() {
		return this.remetente + this.destinatario + this.assunto + this.corpo + this.prioridade;
	}
	
	public void insereAssinatura(String assinatura) {
		if(!this.ehAssinado){
			this.corpo = this.corpo + " - " + assinatura;
			this.ehAssinado = true;
		}
	}
	
	public boolean estaAssinado() {
		return this.ehAssinado;
	}
	
	public Email() {
		
	}
	
	public Email(String remetente, String destinatario, String assunto, String corpo, int prioridade) {
		this.remetente = remetente;
		this.destinatario = destinatario;
		this.assunto = assunto;
		this.corpo = corpo;
		this.prioridade = prioridade;
	}

	public String getRemetente() {
		return remetente;
	}

	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getCorpo() {
		return corpo;
	}

	public void setCorpo(String corpo) {
		this.corpo = corpo;
	}

	public int getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}

	public boolean getEhAssinado() {
		return ehAssinado;
	}

	public void setEhAssinado(boolean ehAssinado) {
		this.ehAssinado = ehAssinado;
	}

}
