package br.edu.unisinos.lab1.aula6ate8.listaLacos1;

public class Ex15 {

	public static void main(String[] args) {
		boolean ehPrimo = true;
		int soma = 0;

		for (int num = 0; num < 200; num++) {
			ehPrimo = true;
			if(num!=0 && num!=1){
				for (int i = num; i > 2 && ehPrimo; i--) {
					if(num % (i-1) == 0) {ehPrimo = false;}
				}
			} else {ehPrimo = false;}
			if(ehPrimo){
				System.out.println(num);
				soma+= num;
			}
		}
		System.out.println("Soma dos n�meros: " + soma);
	}

}
