package br.edu.unisinos.lab1.aula6ate8.listaLacos1;

import auxiliares.Teclado;

public class Ex14 {

	public static void main(String[] args) {
		Teclado leitor = new Teclado();
		boolean ehPrimo = true;
		int num;
		
		do{			
			num = leitor.leInt("Digite um n�mero natural: ");
		}while(num < 0 );
		
		if(num!=0 && num!=1){
			for (int i = num; i > 2 && ehPrimo; i--) {
				if(num % (i-1) == 0) {ehPrimo = false;}
			}
		} else {ehPrimo = false;}
		System.out.println(ehPrimo ? "� Primo" : "N�o � primo");
	}

}
