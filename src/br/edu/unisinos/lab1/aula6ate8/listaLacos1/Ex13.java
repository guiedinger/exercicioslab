package br.edu.unisinos.lab1.aula6ate8.listaLacos1;

import auxiliares.Teclado;

public class Ex13 {

	public static void main(String[] args) {
		Teclado leitor = new Teclado();
		int num, fatorial = 0;
		
		do{			
			num = leitor.leInt("Digite um n�mero positivo: ");
		}while(num < 0 );
		
		fatorial = num;
		for (int i = num; i > 1; i--) {
			fatorial = fatorial*(i-1);
		}
		System.out.println("Fatorial: " + fatorial);

	}

}
