package br.edu.unisinos.lab1.aula6ate8.listaLacos1;

public class PrimoEficiente {

	public static void main(String[] args) throws InterruptedException {
		boolean ehPrimo = true;
		int soma = 2, cont = 0;
		int primos[] = new int[10000000];
		primos[0] = 2;

		for (int num = 1; num < 1000000000; num+=2) {
			ehPrimo = true;
			if (num != 0 && num != 1) {
				for (int i = 0; i < cont && ehPrimo && i < (int)Math.sqrt(num); i++ ){
					if (num % primos[i] == 0) {
						ehPrimo = false;
					}
				}
			} else {
				ehPrimo = false;
			}
			if (ehPrimo) {
				System.out.println(num);
				cont++;
				primos[cont] = num;
				soma += num;
				//Thread.sleep(100);
			}
		}
		System.out.println(primos[cont]);
		System.out.println("Soma dos n�meros: " + soma);

	}

}
