package br.edu.unisinos.lab1.aula3ate5.enunciadoE1;

public class Aluno {
	
	public String nome;
	public double grauA, grauB, grauC, mediaFinal;
	
	public Aluno (String nome){
		this.nome = nome;
		double grauA1 = (Math.random()*10) + 1 ;
		this.grauA = grauA1 > 10 ? 10 : grauA1;
		double grauB1 = (Math.random()*10) + 1 ;
		this.grauB = grauB1 > 10 ? 10 : grauB1;
		double grauC1 = (Math.random()*10) + 1 ;
		this.grauC = grauC1 > 10 ? 10 : grauC1;
	}
	
	public double calculaMedia(){
		double menor = this.grauA < this.grauB ? this.grauA : this.grauB;
		double media = ((this.grauA+this.grauB*2)/3);
		return this.mediaFinal =  media > 6 ? media : this.grauA == menor ? ((this.grauC+this.grauB*2)/3) : ((this.grauA+this.grauC*2)/3);
	}
	
	public String getConceito() {
		double media = calculaMedia();		
		if(media < 6){
			return "Insuficiente";
		} else if(media >= 6 && media < 7){
			return "Regular";
		} else if(media >= 7 && media < 8){
			return "Bom";
		} else if(media >= 8 && media < 9){
			return "Muito bom";
		} else if(media >= 9){
			return "Otimo";
		}
		return "M�dia invalida";
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getGrauA() {
		return grauA;
	}

	public void setGrauA(double grauA) {
		this.grauA = grauA;
	}

	public double getGrauB() {
		return grauB;
	}

	public void setGrauB(double grauB) {
		this.grauB = grauB;
	}

	public double getGrauC() {
		return grauC;
	}

	public void setGrauC(double grauC) {
		this.grauC = grauC;
	}

	public double getMediaFinal() {
		return mediaFinal;
	}

	public void setMediaFinal(double mediaFinal) {
		this.mediaFinal = mediaFinal;
	}

	public static void main(String[] args) {
		Aluno aluno1 = new Aluno("Guilherme");
		Aluno aluno2 = new Aluno("Joao");
		Aluno aluno3 = new Aluno("Josefina");
		
		System.out.printf("Nome: " + aluno1.getNome() + "\nConceito: " + aluno1.getConceito() + "\nM�dia final: %.2f", aluno1.calculaMedia());
		System.out.printf("\n\n\nNome: " + aluno2.getNome() + "\nConceito: " + aluno2.getConceito() + "\nM�dia final: %.2f", aluno2.calculaMedia());
		System.out.printf("\n\n\nNome: " + aluno3.getNome() + "\nConceito: " + aluno3.getConceito() + "\nM�dia final: %.2f", aluno3.calculaMedia());

	}

}
