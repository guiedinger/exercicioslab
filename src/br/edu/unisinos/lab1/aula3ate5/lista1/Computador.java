package br.edu.unisinos.lab1.aula3ate5.lista1;

public class Computador {
	private double preco;
	private String marca;
	private int anoFabricacao;
	private boolean eNovo;
	
	public void imprimeInfo() {
		System.out.println("Marca: " + this.marca +
							"\nPre�o: " + this.preco + 
							"\nAno Fabrica��o: " + this.anoFabricacao +
							"\nNovo: " + (this.eNovo ? "Sim" : "N�o"));
	}

	public Computador(double preco, String marca, int anoFabricacao, boolean eNovo) {
		super();
		this.preco = preco;
		this.marca = marca;
		this.anoFabricacao = anoFabricacao;
		this.eNovo = eNovo;
	}
	
	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(int anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public boolean iseNovo() {
		return eNovo;
	}

	public void seteNovo(boolean eNovo) {
		this.eNovo = eNovo;
	}

	public static void main(String[] args) {
		Computador computador = new Computador(2300, "lenovo", 2013, false);
		computador.imprimeInfo();

	}

}
