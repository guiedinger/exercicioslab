package br.edu.unisinos.lab1.aula3ate5.lista1;

public class Principal {

	public static void main(String[] args) {
		//a
		Aluno alunoBra = new Aluno("Brandamente Brasil", "1585248", 5, 9);
		Aluno alunoRad = new Aluno("Radigunda Cercen�", "2254879", 8, 2);
		Aluno alunoVit = new Aluno("Vitimado Jos� Ara�jo", "0085994", 7, 1);
		
		//b
		alunoBra.imprimeInfo();
		alunoRad.imprimeInfo();
		alunoVit.imprimeInfo();
		
		//c
		alunoRad.setNotaGa(9);
		
		//d
		System.out.println(alunoRad.calculaMediaFinal());
		
		//e
		System.out.println(alunoVit.getMatricula());
		
		//f
		alunoBra.setMatricula("1585228");
		
		//g
		Pessoa pessoaEx = new Pessoa("Excelsa", "Feminino", 19);
		Pessoa pessoaPa = new Pessoa("Pac�fico", "Masculino", 102);
		
		//h
		pessoaEx.aumentaIdade();
		
		//i
		pessoaEx.imprimeInfo();
		pessoaPa.imprimeInfo();
		
		
	}

}
