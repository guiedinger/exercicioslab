package br.edu.unisinos.lab1.aula3ate5.lista1;

public class Aluno {
	private String nome, matricula;
	private double notaGa, notaGb;
	
	public double calculaMediaFinal(){
		return (this.notaGa*0.33)+(this.notaGb*0.67);
	}
	
	public void imprimeInfo(){
		System.out.println("Nome: " + this.nome + "\n" +
							"Matr�cula: " + this.matricula + "\n" +
							"Nota GA: " + this.notaGa + "\n" +
							"Nota GB: " + this.notaGb + "\n" +
							"M�dia final: " + this.calculaMediaFinal());
	}
	
	public Aluno(String nome, String matricula, double notaGa, double notaGb) {
		super();
		this.nome = nome;
		this.matricula = matricula;
		this.notaGa = notaGa;
		this.notaGb = notaGb;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public double getNotaGa() {
		return notaGa;
	}

	public void setNotaGa(double notaGa) {
		this.notaGa = notaGa;
	}

	public double getNotaGb() {
		return notaGb;
	}

	public void setNotaGb(double notaGb) {
		this.notaGb = notaGb;
	}

	public static void main(String[] args) {
		Aluno aluno = new Aluno("Guilherme", "0574510154BA", 8.6, 9.4);
		aluno.imprimeInfo();

	}

}
