package br.edu.unisinos.lab1.aula3ate5.lista2;

public class Livro {
	private String nome, autor;
	private double preco;
	private int anoPublicacao;
	
	public void imprimeInfomacoes() {
		System.out.println("Nome: " + this.nome +
							"\nAutor: " + this.autor +
							"\nPre�o: " + this.preco +
							"\nAno de Publica��o: " + this.anoPublicacao);	
	}
	
	public Livro(String nome, String autor, double preco, int anoPublicacao) {
		super();
		this.nome = nome;
		this.autor = autor;
		this.preco = preco;
		this.anoPublicacao = anoPublicacao;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public int getAnoPublicacao() {
		return anoPublicacao;
	}
	public void setAnoPublicacao(int anoPublicacao) {
		this.anoPublicacao = anoPublicacao;
	}

}
