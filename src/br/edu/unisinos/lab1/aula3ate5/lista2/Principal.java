package br.edu.unisinos.lab1.aula3ate5.lista2;

import auxiliares.Teclado;

public class Principal {

	public static void main(String[] args) {
		Teclado leitor = new Teclado();
		//a
		Produto prodBatata = new Produto("Batata doce", 10.9, 100);
		Produto prodDvd = new Produto("DVD Banda Calipso", 0.01, 1000);
		Produto prodLiv = new Produto("Livro de Java", 180.56, 84);
		
		//b
		prodBatata.imprimeInfomacoes();
		prodDvd.imprimeInfomacoes();
		prodLiv.imprimeInfomacoes();
		
		//c
		prodDvd.setPreco(0);
		
		//d
		System.out.println("Nome: " + prodDvd.getNome() +
							"\nPre�o: " + prodDvd.getPreco());
		
		//e
		Animal animal1 = new Animal(leitor.leString("Digite o nome: "), 
				leitor.leString("Digite a ra�a: "), 
				leitor.leChar("Digite o sexo, m ou f : "), 
				leitor.leInt("Digite a quantidade de patas: "));
		Animal animal2 = new Animal(leitor.leString("Digite o nome: "), 
				leitor.leString("Digite a ra�a: "), 
				leitor.leChar("Digite o sexo, m ou f : "), 
				leitor.leInt("Digite a quantidade de patas: "));
		Animal animal3 = new Animal(leitor.leString("Digite o nome: "), 
				leitor.leString("Digite a ra�a: "), 
				leitor.leChar("Digite o sexo, m ou f : "), 
				leitor.leInt("Digite a quantidade de patas: "));
		
		//f
		animal1.imprimeInfomacoes();
		animal2.imprimeInfomacoes();
		animal3.imprimeInfomacoes();
		
		//g
		Livro livro = new Livro(leitor.leString("Digite o nome: "), 
				leitor.leString("Digite o autor: "), 
				leitor.leDouble("Digite o valor: "), 
				leitor.leInt("Digite o ano de publica��o: "));
		
		//h
		livro.imprimeInfomacoes();
				
	}

}
