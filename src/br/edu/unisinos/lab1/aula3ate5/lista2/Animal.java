package br.edu.unisinos.lab1.aula3ate5.lista2;

public class Animal {
	private String nome, raca;
	private char sexo;
	private int qtdPatas;
	
	public void imprimeInfomacoes() {
		System.out.println("Nome: " + this.nome + 
							"\nRaca: " + this.raca + 
							"\nSexo: " + this.sexo +
							"\nQuantidade de patas: " + this.qtdPatas);
	}
	
	public Animal(String nome, String raca, char sexo, int qtdPatas) {
		super();
		if(qtdPatas < 2) {
			System.out.println("Animal nao criado, ele deve ter no minimo 2 patas!");
			return;
		}
		this.nome = nome;
		this.raca = raca;
		this.sexo = sexo;
		this.qtdPatas = qtdPatas;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRaca() {
		return raca;
	}
	public void setRaca(String raca) {
		this.raca = raca;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	public int getQtdPatas() {
		return qtdPatas;
	}
	public void setQtdPatas(int qtdPatas) {
		this.qtdPatas = qtdPatas;
	}
	
}
