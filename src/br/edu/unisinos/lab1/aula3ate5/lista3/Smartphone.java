package br.edu.unisinos.lab1.aula3ate5.lista3;

public class Smartphone {
	private String nome, marca, cor;
	private double tamanhoTela, preco;
	
	public void exibirDados() {
		System.out.println("Nome: " + this.nome + 
							"\nMarca: " + this.marca +
							"\nCor: " + this.cor +
							"\nPre�o: " + this.preco +
							"\nTamanho da tela: " + this.tamanhoTela + " polegadas");	
	}
	
	public Smartphone() {
	}

	public Smartphone(String nome, String cor, double preco) {
		super();
		this.nome = nome;
		this.cor = cor;
		this.preco = preco;
	}

	public Smartphone(String nome, String marca, String cor, double tamanhoTela, double preco) {
		super();
		this.nome = nome;
		this.marca = marca;
		this.cor = cor;
		this.tamanhoTela = tamanhoTela;
		this.preco = preco;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public double getTamanhoTela() {
		return tamanhoTela;
	}
	public void setTamanhoTela(double tamanhoTela) {
		this.tamanhoTela = tamanhoTela;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	
}
