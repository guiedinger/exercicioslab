package br.edu.unisinos.lab1.aula3ate5.lista3;

public class Pais {
	private String nome, capital;
	private double km2;
	
	public void exibirDados() {
		System.out.println("Nome: " + this.nome +
							"\nCapital: " + this.capital + 
							"\nKm2: " + this.km2);
	}
	
	public Pais() {
	}

	public Pais(String nome, String capital, double km2) {
		super();
		this.nome = nome;
		this.capital = capital;
		this.km2 = km2;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public double getKm2() {
		return km2;
	}

	public void setKm2(double km2) {
		this.km2 = km2;
	}
	
	
	
}
