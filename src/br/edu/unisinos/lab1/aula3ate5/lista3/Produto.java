package br.edu.unisinos.lab1.aula3ate5.lista3;

public class Produto {
	private int codigo, estoque;
	private String nome;
	private double preco;
	
	public void exibirDados() {
		System.out.println("C�digo: " + this.codigo +
							"\nNome: " + this.nome +
							"\nPre�o: " + this.preco +
							"\nEstoque: " + this.estoque);	
	}
	
	public Produto() {
		
	}
	
	public Produto(int codigo, String nome, double preco, int estoque) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.preco = preco;
		this.estoque = estoque;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public int getEstoque() {
		return estoque;
	}
	public void setEstoque(int estoque) {
		this.estoque = estoque;
	}
	
	public static void main(String[] args) {
		//a
		Produto prod1 = new Produto(1 ,"DVD", 1.5, 262);
		Produto prod2 = new Produto();
		
		//b
		prod1.exibirDados();
		prod2.exibirDados();
		
		//c
//		C�digo: 1
//		Nome: DVD
//		Pre�o: 1.5
//		Estoque: 262
//		C�digo: 0
//		Nome: null
//		Pre�o: 0.0
//		Estoque: 0

		
	}
	
}
