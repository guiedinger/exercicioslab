package br.edu.unisinos.lab1.aula3ate5.lista3;

public class Funcionario {
	private String nome;
	private double salario;
	private int numeroDeDependentes;
	
	public void exibirDados() {
		System.out.println("Nome: " + this.nome + 
							"\nSalario: " + this.salario + 
							"\nNumero de dependentes: " + this.numeroDeDependentes);
	}

	public Funcionario() {
		super();
	}

	public Funcionario(String nome, double salario, int numeroDeDependentes) {
		super();
		this.nome = nome;
		this.salario = salario;
		this.numeroDeDependentes = numeroDeDependentes;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public int getNumeroDeDependentes() {
		return numeroDeDependentes;
	}

	public void setNumeroDeDependentes(int numeroDeDependentes) {
		this.numeroDeDependentes = numeroDeDependentes;
	}

	public static void main(String[] args) {
		//a
		Funcionario func = new Funcionario("Guilherme", 15000, 0);
		Funcionario func2 = new Funcionario();
		
		//b
		func.exibirDados();
		func2.exibirDados();
		
		//c
//		Nome: Guilherme
//		Salario: 15000.0
//		Numero de dependentes: 0
//		Nome: null
//		Salario: 0.0
//		Numero de dependentes: 0

	}

}
