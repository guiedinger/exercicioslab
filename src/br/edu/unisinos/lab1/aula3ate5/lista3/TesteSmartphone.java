package br.edu.unisinos.lab1.aula3ate5.lista3;

public class TesteSmartphone {

	public static void main(String[] args) {
		//a
		Smartphone moto = new Smartphone();
		Smartphone ix = new Smartphone();
		Smartphone galax = new Smartphone();
		
		//b
		moto.setMarca("Lenovo");
		moto.setNome("Moto Z PLAY");
		moto.setCor("Preto");
		moto.setPreco(1800);
		moto.setTamanhoTela(5);
		
		ix.setNome("iPhone X");
		ix.setMarca("Apple");
		ix.setCor("Branco");
		ix.setPreco(5000);
		ix.setTamanhoTela(5.7);
		
		galax.setNome("Galaxy S9");
		galax.setMarca("Samsung");
		galax.setCor("Preto");
		galax.setPreco(3790);
		galax.setTamanhoTela(6);
		
		
		//c
		moto.exibirDados();
		ix.exibirDados();
		galax.exibirDados();
		
	}

}
