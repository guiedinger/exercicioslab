package br.edu.unisinos.lab1.aula2;

import auxiliares.Teclado;

/**
 * Escreva a descriÃ§Ã£o da classe ListaExerciciosAula2 aqui.
 * 
 * @author (seu nome) 
 * @version (nÃºmero de versÃ£o ou data)
 */
public class ListaExerciciosAula2
{
    Teclado leitor = new Teclado();
    
    public void ex1(){
        String nome = leitor.leString("Digite seu nome:");
        System.out.println(nome);
    }
    
    public void ex2(){
        String nome = leitor.leString("Digite seu nome:");
        int idade = leitor.leInt("Digite sua idade:");
        System.out.println(nome+" "+idade);
    }
    
    public void ex3(){
        String nome = leitor.leString("Digite seu nome:");
        System.out.println(nome);
        double altura = leitor.leDouble("Digte sua altura:");
        System.out.println(altura);
        System.out.println("Obrigado!");
    }
    
    public void ex4(){
        String rua = leitor.leString("Digite seu Rua:");
        String cep = leitor.leString("Digite seu cep:");
        String bairro = leitor.leString("Digite seu bairro:");
        System.out.println("Rua: " + rua +"\n" +
                            "Cep: " + cep + "\n" +
                            "Bairro: " + bairro);
    }
    
    public void ex5(){
        int num1 = leitor.leInt("Digite o primeiro numero:");
        int num2 = leitor.leInt("Digite o segundo numero:");
        int num3 = leitor.leInt("Digite o terceiro numero:");
        int num4 = leitor.leInt("Digite o quarto numero:");
        int num5 = leitor.leInt("Digite o quinto numero:");
        System.out.println("Soma: " + (num1+num2+num3+num4+num5));
        System.out.println("Produto: " + (num1*num2*num3*num4*num5));
    }
    
    public void ex6(){
        int a = leitor.leInt("Digite o primeiro numero:");
        int b = leitor.leInt("Digite o segundo numero:");
        int c = leitor.leInt("Digite o terceiro numero:");
        int d = leitor.leInt("Digite o quarto numero:");
        int e = leitor.leInt("Digite o quinto numero:");
        System.out.println("Ã�rea triangulo: " + (b*c)/2);
        System.out.println("Perimetro: " + (a+b+c+d));
        System.out.println("Ã�rea circulo: " + (Math.PI*e*e));
    }
    
    public void ex7(){
        double trabalho = leitor.leDouble("Digite a nota do trabalho:");
        double prova = leitor.leDouble("Digite a nota da prova:");
        double teste = leitor.leDouble("Digite a nota do teste:");
        System.out.println("Nota final: " + (trabalho+prova*6+teste*3)/10);
    }
    
    public void ex8(){
        double ativiPA = leitor.leDouble("Digite a nota da Atividade:");
        double ativiTA = leitor.leDouble("Digite a nota da Atividade:");
        double provaB = leitor.leDouble("Digite a nota da Prova:");
        double testeTB = leitor.leDouble("Digite a nota do Teste TeÃ³rico:");
        double trabEB = leitor.leDouble("Digite a nota do Trabalho extraclasse:");
        double ga = (ativiPA*4.5+ativiTA*5.5)/10;
        double gb = (provaB*6+testeTB*2+trabEB*2)/10;
        double notaFinal = (ga+gb*2)/3;
        System.out.println("Nota final: " + notaFinal);
    }
    
    public ListaExerciciosAula2(){}
}
