package br.edu.unisinos.lab1.extras;
import auxiliares.Teclado;
public class ExtraAula2 {
	private Teclado leitor = new Teclado();
	public void ex1(){
		double comprimento, 
		largura, 
		altura, 
		paredesComp, 
		paredesLarg, 
		metragemTotal; 
		int caixas;
		
		comprimento = leitor.leDouble("Comprimento:");
		largura = leitor.leDouble("Largura:");
		altura = leitor.leDouble("Altura:");
		
		paredesComp = altura*comprimento*2;
		paredesLarg = altura*largura*2;
		metragemTotal = paredesComp+paredesLarg;
		caixas = (int) 
				(metragemTotal % 1.5 == 0 ? 
						metragemTotal/1.5 : (metragemTotal/1.5) + 1);
		
		System.out.println("Total de Caixas:" + caixas);
	}
	
	public void ex2(){
		int eleitores, brancos, nulos, validos;
		double brancosP, nulosP, validosP; //P = porcentagem
		
		eleitores = leitor.leInt("Eleirores:");
		brancos = leitor.leInt("Brancos:");
		nulos = leitor.leInt("Nulos");
		validos = leitor.leInt("Validos:");
		
		if(brancos+nulos+validos> eleitores){
			System.out.println("O numero de votos precisa ser menor que o de eleitores!");
		}else{
		brancosP = (brancos*100)/500;
		nulosP = (nulos*100)/500;
		validosP = (validos*100)/500;
		
		System.out.println("Brancos: " + brancosP + "%");
		System.out.println("Nulos: " + nulosP + "%");
		System.out.println("Validos: " + validosP + "%");
		}
	}
	
	public void ex3(){
		int empregados, vendasBike;
		double salarioMin, custoBike, 
		salarioEmpregado, lucroLiquido, 
		vendas, comissao;
		
		empregados = leitor.leInt("Quantidade de empregados:");
		vendasBike = leitor.leInt("Quantidade de bicletas vendidas:");
		salarioMin = leitor.leDouble("Valor do sal�rio min�mo:");
		custoBike = leitor.leDouble("Valor de custo da bicileta:");
		
		vendas = (custoBike*1.5)*vendasBike;
		comissao = (custoBike*0.15)*vendasBike;
		salarioEmpregado = (salarioMin*2) + (comissao/empregados);
		lucroLiquido = vendas - salarioEmpregado*empregados;
		
		System.out.println("Sal�rio empregado: " + salarioEmpregado);
		System.out.println("Lucro Liquido: " + lucroLiquido);
	}
	
	public void ex4(){
		double precoCombustivel = 0.9,
				odometroInicio, 
				odometroFim,
				litrosGastos,
				valorTotal,
				lucroLiquido,
				mediaConsumo;
		
		odometroInicio = leitor.leDouble("Odometro inicio:");
		odometroFim = leitor.leDouble("Odometro fim:");
		litrosGastos = leitor.leDouble("Litros gastos:");
		valorTotal = leitor.leDouble("Valor total recebido:");
		
		lucroLiquido = valorTotal - (litrosGastos*precoCombustivel);
		mediaConsumo = (odometroFim-odometroInicio)/litrosGastos;
		
		System.out.println("Media consumo: " + mediaConsumo);
		System.out.println("Lucro liquido: " + lucroLiquido);
		
	}
	public void ex5(){
		double valor1, valor2, valor3, soma;
		
		valor1 = leitor.leDouble("Valor 1:");
		valor2 = leitor.leDouble("Valor 2:");
		valor3 = leitor.leDouble("Valor 3:");
		
		soma = valor1 + valor2 + valor3;
		
		System.out.println("Soma: " + soma);
	}
	
	public void ex6(){
		double valor1, valor2, valor3, soma, produto, produtoFinal;
		
		valor1 = leitor.leDouble("Valor 1:");
		valor2 = leitor.leDouble("Valor 2:");
		valor3 = leitor.leDouble("Valor 3:");
		
		soma = valor1 + valor2;
		produto = valor2 + valor3;
		produtoFinal = soma*produto;
		
		System.out.println("Total: " + produtoFinal);
	}
	
	public void ex7(){
		double a, b, c, d, e, f, x, y;
		
		a = leitor.leDouble("A:");
		b = leitor.leDouble("B:");
		c = leitor.leDouble("C:");
		d = leitor.leDouble("D:");
		e = leitor.leDouble("E:");
		f = leitor.leDouble("F:");
		
		if(a==0 || e==0 || b==0 || d==0){
			System.out.println("Os coeficientes A, E, B e D n�o podem ser 0 !");
		}else{
			x = ((c * e) - (b * f))/((a * e) - (b * d));
			y = ((a * f) - (c * d))/((a * e) - (b * d));
			
			System.out.println("X = " + x);
			System.out.println("Y = " + y);
		}
	}
	public static void main(String[] args) {
		ExtraAula2 obj = new ExtraAula2();
		obj.ex5();
	}

}
