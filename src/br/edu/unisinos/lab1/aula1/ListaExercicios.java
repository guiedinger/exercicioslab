package br.edu.unisinos.lab1.aula1;


public class ListaExercicios
{
    public void ex1(){
        System.out.println("Alô mundo");
    }
    
    public void ex2(){
        int num0 = 1;
        int num1 = 1;
        int soma = num0+num1;
        System.out.println("Soma: " + soma);
    }
    
    public void ex3(){
        int saldoInicial = 5;
        int receitas = 2;
        int despesas = 3;
        int saldoFinal = saldoInicial + (receitas-despesas);
        System.out.println("Saldo final: " + saldoFinal);
    }
    
    public void ex4(){
        int num = 2;
        double raiz = Math.sqrt(num);
        System.out.println("Raiz: " + raiz);
    }
    
    public void ex5(){
        int raio = 3;
        int altura = 3;
        double volume = Math.PI*(raio*raio)*altura;
        System.out.println("Volume: " + volume);        
    }
    
    public void ex6(){
        int a = 2;
        int b = 3;
        int temp = a;
        a = b;
        b = temp;
        System.out.println("A: " + a + " B: " + b);  
    }
    
    public void ex7(){
        double grausCent = 20;
        double grausFahren = (9*grausCent+160)/5;
        System.out.println("Graus Fahren: " + grausFahren);
    }
    
    public void ex8(){
    double ga = 8;
    double gb = 9;
    double media = (ga + 2*gb)/3;
    System.out.println("Média: " + media);
    }
    
    public ListaExercicios()
    {
    }


}
