package br.edu.unisinos.lab1.aula9ate12.teste2;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import auxiliares.Teclado;

public class Audiencia {
	private Teclado leitor = new Teclado();
	public void calculaAudiencia() {
		DecimalFormat df = new DecimalFormat("#.#");
		DecimalFormat dfo = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.UP);
		String saida = "S";
		
		while(saida.equalsIgnoreCase("S")) {
			int osci = 0, cinq = 0, qtd = 0;
			double soma = 0, temp = 0, valor = 0;
			qtd = leitor.leInt("Digite a quantidade de �ndices de audi�ncia ser�o digitados:");
			for (int i = 0; i < qtd; i++) {
				do{
					if(!(valor >= 0 && valor <= 100)){
						System.out.println("Valor fora da faixa de 0 � 100.");
					}
					valor = leitor.leDouble();				
				}while(!(valor >= 0 && valor <= 100));
				soma+=valor;
				if(i > 0 && valor < temp) {
					osci++;
				}
				temp = valor;
				if(valor > 50) {
					cinq++;
				}
			}
			if(osci == 0) {
				System.out.println("AUDI�NCIA SEMPRE CRESCENTE.");
			} else {
				System.out.println("AUDI�NCIA NEM SEMPRE CRESCENTE.");
				System.out.println("Quantas oscila��es:"+osci);
			}
			System.out.println("M�dia de audi�ncia:" + df.format(soma/qtd));
			System.out.println(dfo.format((double)cinq*100/qtd) + "% das audi�ncias acima de  50.");
			saida = leitor.leString("Deseja continuar? S ou N");
		}
	}

}
