package br.edu.unisinos.lab1.aula9ate12.trabalhoGB;

public class GList< T > {
	private T[] list;
	private int limit, qtd;
	
	@SuppressWarnings("unchecked")
	public GList(int limit) {
		this.list = (T[])new Object[limit];
		this.limit = limit;
		this.qtd = 0;
	}
	
	public boolean add(T element){
		if(qtd < limit){
			this.list[qtd] = element;
			this.qtd++;
			return true;
		}
		return false;
	}
	
	public boolean removeByIndex(int index) {
		if(index >= 0 && index <= this.qtd-1){
			if(index == qtd-1){
				this.qtd--;
			} else {
				this.list[index] = this.list[--qtd];				
			}
			return true;
		}
		return false;
	}
	
	public boolean updateByIndex(int index, T element) {
		if(index >= 0 && index <= this.qtd-1){
				this.list[index] = element;
			return true;
		}
		return false;
	}
	
	public void printList() {
		for (int i = 0; i < this.qtd; i++) {
			System.out.println(this.list[i].toString());
		}
	}

	public T[] getList() {
		return this.list;
	}

	public int getLimit() {
		return limit;
	}

	public int getQtd() {
		return qtd;
	}	
	
}
