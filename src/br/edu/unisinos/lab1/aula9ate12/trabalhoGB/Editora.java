package br.edu.unisinos.lab1.aula9ate12.trabalhoGB;

public class Editora {
	private String nomeEditora;
	private int codEditora;
	
	public String getEditora() {
		return nomeEditora;
	}

	public void setEditora(String nomeEditora) {
		this.nomeEditora = nomeEditora;
	}

	public int getCodEditora() {
		return codEditora;
	}

	public void setCodEditora(int codEditora) {
		this.codEditora = codEditora;
	}

	@Override
	public String toString() {
		return "Editora [nomeEditora=" + nomeEditora + ", codEditora=" + codEditora + "]";
	}

	public Editora(String nomeEditora, int codEditora){
		this.nomeEditora = nomeEditora;
		this.codEditora = codEditora;
	}

	

}
