package br.edu.unisinos.lab1.aula9ate12.trabalhoGB;

public class Biblioteca {
	private GList<Livro> listaLivro;
	private GList<Editora> listaEditora;

	public Biblioteca(int tamLivro, int tamEditora) {
		this.listaLivro = new GList<Livro>(tamLivro);
		this.listaEditora = new GList<Editora>(tamEditora);
	}

	public void listaLivros() {
		Object[] list = this.listaLivro.getList();
		for (int i = 0; i < this.listaLivro.getQtd(); i++) {
			System.out.println(((Livro) list[i]).toString() + ", nomeEditora="
					+ buscaEditoraPeloCodigo(((Livro) list[i]).getCodEditora()).getEditora() + "]");
		}
	}

	public void listaEditoras() {
		this.listaEditora.printList();
	}

	public boolean insereLivro(Livro livro) {
		if (buscaEditoraPeloCodigo(livro.getCodEditora()) != null) {
			return listaLivro.add(livro);
		}
		return false;
	}

	public boolean insereEditora(Editora editora) {
		if (buscaEditoraPeloCodigo(editora.getCodEditora()) == null) {
			return listaEditora.add(editora);
		}
		return false;
	}

	public boolean removeLivroPeloNome(String nome) {
		Object[] list = this.listaLivro.getList();
		for (int i = 0; i < this.listaLivro.getQtd(); i++) {
			if (((Livro) list[i]).getNomeLivro().equalsIgnoreCase(nome)) {
				this.listaLivro.removeByIndex(i);
				return true;
			}
		}
		return false;
	}

	public boolean removeLivroPelaEditora(int codigo) {
		Object[] list = this.listaLivro.getList();
		int cont = 0;
		for (int i = 0; i < this.listaLivro.getQtd(); i++) {
			if (((Livro) list[i]).getCodEditora() == codigo) {
				this.listaLivro.removeByIndex(i--);
				cont++;
			}
		}
		return cont > 0 ? true : false;
	}

	public boolean removeEditora(int codigo) {
		Object[] list = this.listaEditora.getList();
		for (int i = 0; i < this.listaEditora.getQtd(); i++) {
			if (((Editora) list[i]).getCodEditora() == codigo) {
				if (this.listaEditora.removeByIndex(i)) {
					this.removeLivroPelaEditora(codigo);
					return true;
				}
			}
		}
		return false;
	}

	private Editora buscaEditoraPeloCodigo(int codigo) {
		Object[] list = this.listaEditora.getList();
		for (int i = 0; i < this.listaEditora.getQtd(); i++) {
			if (((Editora) list[i]).getCodEditora() == codigo) {
				return (Editora) list[i];
			}
		}
		return null;
	}

}
