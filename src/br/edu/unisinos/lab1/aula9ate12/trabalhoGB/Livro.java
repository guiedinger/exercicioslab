package br.edu.unisinos.lab1.aula9ate12.trabalhoGB;

public class Livro {
	private String nomeLivro, autor;
	private int qtdPaginas, codEditora;
	
	public Livro (String nomeLivro, String autor, int codEditora, int qtdPaginas) {
		this.nomeLivro = nomeLivro;
		this.autor = autor;
		this.codEditora = codEditora;
		this.qtdPaginas = qtdPaginas;
	}

	public String getNomeLivro() {
		return nomeLivro;
	}

	public void setNomeLivro(String nomeLivro) {
		this.nomeLivro = nomeLivro;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getCodEditora() {
		return codEditora;
	}

	public void setCodEditora(int codEditora) {
		this.codEditora = codEditora;
	}

	public int getQtdPaginas() {
		return qtdPaginas;
	}

	public void setQtdPaginas(int qtdPaginas) {
		this.qtdPaginas = qtdPaginas;
	}

	@Override
	public String toString() {
		return "Livro [nomeLivro=" + nomeLivro + ", autor=" + autor + ", qtdPaginas="
				+ qtdPaginas;
	}
	
	
	
}
