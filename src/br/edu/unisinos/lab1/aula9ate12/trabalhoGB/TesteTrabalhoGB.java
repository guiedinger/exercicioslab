package br.edu.unisinos.lab1.aula9ate12.trabalhoGB;

import auxiliares.Teclado;

public class TesteTrabalhoGB {

	public static void main(String[] args) {
		Teclado leitor = new Teclado();
		boolean continuar = true;
		Biblioteca b = new Biblioteca(20, 10);
		
		System.out.println("Bem vindo a Biblioteca!\n");
		
		do {
			System.out.println(
			"\nDigite o numero da op��o que deseja:\n" 
			+ "1 - Insere Livro;\n" 
			+ "2 - Remove Livro pelo nome;\n"
			+ "3 - Remove Livro pela editora;\n" 
			+ "4 - Lista Livros;\n" 
			+ "5 - Insere Editora;\n"
			+ "6 - Remove Editora;\n" 
			+ "7 - Lista Editoras;\n" 
			+ "8 - Sair da biblioteca.");
			
			int pos = leitor.leInt();
			boolean valid = true;
			switch (pos) {
			case 1: // Insere Livro
				valid = b.insereLivro(new Livro(
						leitor.leString("Nome:"), 
						leitor.leString("Autor:"),
						leitor.leInt("Codigo Editora:"), 
						leitor.leInt("Quantidade p�ginas:")));
				if (!valid)
					System.out.println("Editora informada n�o existe!");
				else
					System.out.println("Livro inserido com sucesso!");
				break;
			case 2: // Remove Livro pelo nome
				valid = b.removeLivroPeloNome(leitor.leString("Nome: "));
				if (!valid)
					System.out.println("Livro informado n�o existe!");
				else
					System.out.println("Livro removido com sucesso!");
				break;
			case 3: // Remove Livro pela editora
				valid = b.removeLivroPelaEditora(leitor.leInt("Codigo: "));
				if (!valid)
					System.out.println("Nenhum livro removido!");
				else
					System.out.println("Livro(s) removido(s) com sucesso!");
				break;
			case 4: // Lista livros
				b.listaLivros();
				break;
			case 5: // Insere Editora
				valid = b.insereEditora(new Editora(
						leitor.leString("Nome editora:"), 
						leitor.leInt("Codigo editora:")));
				if (!valid)
					System.out.println("Editora informada j� existe!");
				else
					System.out.println("Editora inserida com sucesso!");
				break;
			case 6: // Remove Editora
				valid = b.removeEditora(leitor.leInt("Codigo: "));
				if (!valid)
					System.out.println("Editora informada n�o existe!");
				else
					System.out.println("Editora(s) removida(s) com sucesso!");
				break;
			case 7: // Lista editoras
				b.listaEditoras();
				break;
			case 8:
				continuar = false;
				System.out.println("Volte sempre!");
				break;
			}

		} while (continuar);

	}

}
