package br.edu.unisinos.lab1.aula9ate12.calculadora;

import auxiliares.Teclado;

public class Calculadora {
	
	
	
	public int converteDecimal(String valor, int base) {
		if(base >= 2 && base <= 36){
			valor = valor.toUpperCase();
			int cont = valor.length() - 1, soma = 0;
			for (int i = 0; i < valor.length() ; i++) {
				if(Character.isDigit(valor.charAt(i))){
					if((valor.charAt(i) - 48) +1 > base) {
						System.out.println("Valor inv�lido!");
						return 0;
					} else {
					soma+= (valor.charAt(i) - 48)*(Math.pow(base, cont--));
					}
				} else {
					if((valor.charAt(i) - 55) +1 > base) {
						System.out.println("Valor inv�lido!");
						return 0;
					} else {
						soma+= (valor.charAt(i) - 55)*(Math.pow(base, cont--));	
					}				
				}
			}
			return soma;		
		}
		System.out.println("Base inv�lida, selecione uma base entre 2 e 36.");
		return 0;
	}
	
	
	public void geraHistograma() {
		Teclado leitor = new Teclado();
		int x = leitor.leInt("Quantos valores?");
		int A = 0, B = 0, C = 0, D = 0, E = 0;
		
		for(int i = 0; i < x; i++) {
			int temp = leitor.leInt("Valor " + i + " :");
			if(temp >= 0 && temp <= 19){
				A++;
			} else if(temp >= 20 && temp <= 39){
				B++;
			} else if(temp >= 40 && temp <= 59){
				C++;
			} else if(temp >= 60 && temp <= 79){
				D++;
			} else if(temp >= 80 && temp <= 99){
				E++;
			}
		}
		
		System.out.println("[00 a 19]: " + (double)(A*100)/x);
		System.out.println("[20 a 39]: " + (double)(B*100)/x);
		System.out.println("[40 a 59]: " + (double)(C*100)/x);
		System.out.println("[60 a 69]: " + (double)(D*100)/x);
		System.out.println("[80 a 89]: " + (double)(E*100)/x);
	}

	public static void main(String[] args) {


		Calculadora calc = new Calculadora();
		
		//System.out.println(calc.converteDecimal("AB2", 16));
		
		calc.geraHistograma();

	}

}
