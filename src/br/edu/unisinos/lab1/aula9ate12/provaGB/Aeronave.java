package br.edu.unisinos.lab1.aula9ate12.provaGB;

public class Aeronave extends Aero{
	
	String modelo;

	public Aeronave(int codigoInternacionalFabricante, String nome, String modelo) {
		super(codigoInternacionalFabricante, nome);
		this.modelo = modelo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	@Override
	public String toString() {
		return "Aeronave: Nome = " + nome + ", Modelo = " + modelo + ", chaveId=" + chaveId + ", C�digo Internacional da Fabricante = "
				+ codigoInternacionalFabricante;
	}
	
}
