package br.edu.unisinos.lab1.aula9ate12.provaGB;

public class Fabricante extends Aero {
	
	String pais;
	
	public Fabricante(int codigoInternacionalFabricante, String nome, String pais) {
		super(codigoInternacionalFabricante, nome);
		this.pais = pais;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	@Override
	public String toString() {
		return "Fabricante: Nome = " + nome + ", Pa�s = " + pais + ", chaveId = " + chaveId + ", C�digo Internacional da Fabricante = "
				+ codigoInternacionalFabricante;
	}
	
	public String toStringAeronave() {
		return ", Nome do fabricante = " + nome + ", Pa�s do fabricante = " + pais;
	}

}
