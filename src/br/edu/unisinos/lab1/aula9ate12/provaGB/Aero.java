package br.edu.unisinos.lab1.aula9ate12.provaGB;

public class Aero {
	
	int chaveId, codigoInternacionalFabricante;
	String nome;
	
	
	public Aero(int codigoInternacionalFabricante, String nome) {
		super();
		this.chaveId = geraChaveId();
		this.codigoInternacionalFabricante = codigoInternacionalFabricante;
		this.nome = nome;
	}
	public int getChaveId() {
		return chaveId;
	}
	public void setChaveId(int chaveId) {
		this.chaveId = chaveId;
	}
	public int geraChaveId() {
		return (int)(Math.random()*1000)+1;
	}
	public int getCodigoInternacionalFabricante() {
		return codigoInternacionalFabricante;
	}
	public void setCodigoInternacionalFabricante(int codigoInternacionalFabricante) {
		this.codigoInternacionalFabricante = codigoInternacionalFabricante;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
}
