package br.edu.unisinos.lab1.aula9ate12.provaGB;

import br.edu.unisinos.lab1.aula9ate12.provaGB.GList;
import auxiliares.Teclado;

public class GerenciadorDeAeroporto {
	private GList<Aeronave> listaAeronave;
	private GList<Fabricante> listaFabricante;

	public GerenciadorDeAeroporto(int tamAeronave, int tamFabricante) {
		this.listaAeronave = new GList<Aeronave>(tamAeronave);
		this.listaFabricante = new GList<Fabricante>(tamFabricante);
	}

	public void exibeAeronaves() {
		Object[] list = this.listaAeronave.getList();
		for (int i = 0; i < this.listaAeronave.getQtd(); i++) {
			System.out.println(((Aeronave) list[i]).toString()
					+ buscaFabricantePeloCodigo(((Aeronave) list[i]).getCodigoInternacionalFabricante()).toStringAeronave());
		}
	}

	public void exibeFabricantes() {
		this.listaFabricante.printList();
	}

	public boolean insereFabricante(Fabricante fabricante) {
		if (buscaFabricantePeloCodigo(fabricante.codigoInternacionalFabricante) == null) {
			return listaFabricante.add(fabricante);
		}
		return false;
	}

	public boolean insereAeronave(Aeronave aeronave) {
		if (buscaFabricantePeloCodigo(aeronave.codigoInternacionalFabricante) != null) {
			return listaAeronave.add(aeronave);
		}
		return false;
	}
	
	public boolean removeAeronavePeloNome(String nome) {
		Object[] list = this.listaAeronave.getList();
		for (int i = 0; i < this.listaAeronave.getQtd(); i++) {
			if (((Aeronave) list[i]).getNome().equalsIgnoreCase(nome)) {
				this.listaAeronave.removeByIndex(i);
				return true;
			}
		}
		return false;
	}
	
	public boolean removeAeronavePelaFabricante(int codigo) {
		Object[] list = this.listaAeronave.getList();
		int cont = 0;
		for (int i = 0; i < this.listaAeronave.getQtd(); i++) {
			if (((Aeronave) list[i]).getCodigoInternacionalFabricante() == codigo) {
				this.listaAeronave.removeByIndex(i--);
				cont++;
			}
		}
		return cont > 0 ? true : false;
	}
	
	public boolean removeFabricante(int codigo) {
		Object[] list = this.listaFabricante.getList();
		for (int i = 0; i < this.listaFabricante.getQtd(); i++) {
			if (((Fabricante) list[i]).getCodigoInternacionalFabricante() == codigo) {
				if (this.listaFabricante.removeByIndex(i)) {
					this.removeAeronavePelaFabricante(codigo);
					return true;
				}
			}
		}
		return false;
	}

	private Fabricante buscaFabricantePeloCodigo(int codigo) {
		Object[] list = this.listaFabricante.getList();
		for (int i = 0; i < this.listaFabricante.getQtd(); i++) {
			if (((Fabricante) list[i]).getCodigoInternacionalFabricante() == codigo) {
				return (Fabricante) list[i];
			}
		}
		return null;
	}

	public static void main(String[] args) {

		Teclado leitor = new Teclado();
		boolean continuar = true;
		GerenciadorDeAeroporto latam = new GerenciadorDeAeroporto(100, 100);

		
		System.out.println("Bem vindo ao Gerenciador de Aeroporto!!\n\n");

		do {
			System.out.println(
					"\nDigite o numero da op��o que deseja:\n" 
					+ "1 - Insere Aeronave;\n" 
					+ "2 - Remove Aeronave pelo nome;\n"
					+ "3 - Remove Aeronave pela Fabricante;\n" 
					+ "4 - Lista Aeronaves;\n" 
					+ "5 - Insere Fabricante;\n"
					+ "6 - Remove Fabricante;\n" 
					+ "7 - Lista Fabricantes;\n" 
					+ "8 - Sair do gerenciador de aeroporto.");
					
					int pos = leitor.leInt();
					boolean valid = true;
					
					switch (pos) {
					case 1:
						System.out.println("� necess�rio escolher uma fabricante: (n�o esque�a de cadastrar fabricantes!)");
						latam.exibeFabricantes();
						valid = latam.insereAeronave(new Aeronave(leitor.leInt("C�digo internacional da fabricante: "), 
								leitor.leString("Nome: "), leitor.leString("Modelo: ")));
						if(!valid)
							System.out.println("Fabricante informada n�o existe!");
						else
							System.out.println("Aeronave inserida com sucesso!");
						break;
					case 2:
						valid = latam.removeAeronavePeloNome(leitor.leString("Nome: "));
						if (!valid)
							System.out.println("Aeronave informada n�o existe!");
						else
							System.out.println("Aeronave removida com sucesso!");
						break;
					case 3:
						valid = latam.removeAeronavePelaFabricante(leitor.leInt("C�digo Internacional da Fabricante: "));
						if (!valid)
							System.out.println("Nenhuma aeronave removida!");
						else
							System.out.println("Aeronave(s) removida(s) com sucesso!");
						break;
					case 4:
						latam.exibeAeronaves();
						break;
					case 5:
						valid = latam.insereFabricante(new Fabricante(leitor.leInt("C�digo internacional da fabricante: "),
								leitor.leString("Nome: "), leitor.leString("Pais: ")));
						if(!valid)
							System.out.println("J� existe uma fabricante com o mesmo c�digo informado!");
						else
							System.out.println("Fabricante inserida com sucesso!");
						break;
					case 6:
						valid = latam.removeFabricante(leitor.leInt("C�digo internacional da fabricante: "));
						if (!valid)
							System.out.println("Fabricante informada n�o existe!");
						else
							System.out.println("Fabricante removida com sucesso!");
						break;
					case 7:
						latam.exibeFabricantes();
						break;
					case 8:
						continuar = false;
						System.out.println("Volte sempre ao gerenciador de aeroportos!");
						break;
					default:
						break;
					}
			

		} while (continuar);

	}

}
